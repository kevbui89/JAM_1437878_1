package jam.test;

import jam.business.AppGroupRecImpl;
import jam.entities.AppGroupRecord;
import jam.entities.Configuration;
import jam.manager.PropertiesManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Kevin
 * @version 1.0
 */
public class AppGroupRecDAOImplTest {
    
    private static final Logger log = LoggerFactory.getLogger(AppGroupRecDAOImplTest.class);
    private Configuration config;
    private AppGroupRecImpl appGroupRecImpl;
    
    @Before
    public void setUp() throws SQLException, IOException {
        PropertiesManager pm = new PropertiesManager();
        config = new Configuration();
        config = pm.loadTextProperties("src/main/resources", "JamDatabaseProperties", config);

        appGroupRecImpl = new AppGroupRecImpl(config);        
    }
    
    /**
     * This will test if a group record is inserted properly in the database
     * with valid data
     *
     * @throws SQLException
     */
    @Test(timeout = 1000)
    public void testInsertGroupAppRec() throws SQLException {
        AppGroupRecord agr = new AppGroupRecord(6, "group test", "#008000");

        appGroupRecImpl.createAppGroupRec(agr);
        
        // Getting the actual appointment group record
        AppGroupRecord agrTest = appGroupRecImpl.findAppGroupRec(agr);
        
        assertEquals("The objects should be the same", agr, agrTest);
    }
    
    /**
     * This will test the deletion of an appointment group record
     *
     * @throws SQLException
     */
    @Test(timeout = 1000)
    public void testDeleteAppGroupRec() throws SQLException {
        AppGroupRecord agr = new AppGroupRecord(5, "Random Team", "#FFA500");
        
        int rec = appGroupRecImpl.deleteAppGroupRec(agr);
        assertEquals("Record deleted should be 1", 1, rec);
    }
    
    /**
     * This will test the update appointment group record table
     *
     * @throws SQLException
     */
    @Test(timeout = 2000)
    public void testUpdateAppGroupRec() throws SQLException {
        AppGroupRecord newAgr = new AppGroupRecord(5, "Random Update", "#FFA500");
        
        int rec = appGroupRecImpl.updateAppGroupRec(newAgr);
        assertEquals("Record updated should be 1", 1, rec);
    }
    
    /**
     * This will test the find appointment group method
     *
     * @throws SQLException
     */
    @Test(timeout = 2000)
    public void testFindAppGroupRec() throws SQLException {
        AppGroupRecord agr = new AppGroupRecord(1, "Magicians", "#FF0000");
        AppGroupRecord group = appGroupRecImpl.findAppGroupRec(agr);
        assertEquals("Appoint record found should be 1", agr, group);
    }

    /**
     * Should throw an IllegalArgumentException for negative value for a color
     *
     * @throws SQLException
     */
    @Test(timeout = 2000, expected = SQLException.class)
    public void testWrongFormatColor() throws SQLException {
        AppGroupRecord agr = new AppGroupRecord();
 
        agr.setGroupName("Test1FormatColor");
        agr.setColor("00800000");
        
        appGroupRecImpl.createAppGroupRec(agr);
        // If an exception was not thrown then the test failed
        fail("A 7 character string is not expected for group color with create method, should throw an IllegalArgumentException");       
    }
    
    @AfterClass
    public static void seedAfterTestCompleted() {
        log.info("@AfterClass seeding");
        new AppGroupRecDAOImplTest().seedDatabase();
    }

    /**
     * This routine recreates the database before every test. This makes sure
     * that a destructive test will not interfere with any other test. Does not
     * support stored procedures.
     *
     * This routine is courtesy of Bartosz Majsak, an Arquillian developer at
     * JBoss
     */
    @Before
    public void seedDatabase() {
        log.info("@After seeding");

        final String seedDataScript = loadAsString("CreateJAMTables.sql");
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/jamdb", "kevin", "jampassword");) {
            for (String statement : splitStatements(new StringReader(seedDataScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }

    /**
     * The following methods support the seedDatabse method
     */
    private String loadAsString(final String path) {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
                Scanner scanner = new Scanner(inputStream)) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    private List<String> splitStatements(Reader reader, String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
    }

}
