package jam.test;

import jam.business.AppRecImpl;
import jam.business.SmtpImpl;
import jam.entities.AppRecord;
import jam.entities.Configuration;
import jam.entities.Smtp;
import jam.manager.PropertiesManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Kevin
 * @version 1.0
 */
public class AppRecDAOImplTest {
    
    private static final Logger log = LoggerFactory.getLogger(AppRecDAOImplTest.class);
    private Configuration config;
    private AppRecImpl appRecImpl;
    
    @Before
    public void setUp() throws SQLException, IOException{
        PropertiesManager pm = new PropertiesManager();
        config = new Configuration();
        config = pm.loadTextProperties("src/main/resources", "JamDatabaseProperties", config);

        appRecImpl = new AppRecImpl(config);        
    }
    
     /**
     * This will test if a group record is inserted properly in the database
     * with valid data
     *
     * @throws SQLException
     */
    @Test(timeout = 2000)
    public void testInsertAppRec() throws SQLException {
        AppRecord ar = new AppRecord();
        
        ar.setTitle("test title");
        ar.setLocation("test location123");
        ar.setStartTimeStampValue(Timestamp.valueOf("2017-09-11 08:30:00"));
        ar.setEndTimeStampValue(Timestamp.valueOf("2017-09-11 09:30:00"));
        ar.setDetails("some details");
        ar.setWholeDay(0);
        ar.SetAppGroup(5);
        ar.setAlarm(0);

        int rec = appRecImpl.createAppRec(ar);

        assertEquals("Record created should be 1", 1, rec);
    }
    
    /**
     * Testing find appointment by ID
     * @throws SQLException 
     */
    @Test(timeout = 2000)
    public void testFindAppByTitle() throws SQLException {
        List<AppRecord> list = new ArrayList<AppRecord>();
        AppRecord ar = new AppRecord(1, "Coffee break", "Cafeteria", Timestamp.valueOf("2017-09-11 10:30:00"),
                Timestamp.valueOf("2017-09-11 11:00:00"), "No details", 0, 1, 0);
        log.debug(ar.toString());

        list = appRecImpl.getAppFromTitle(ar);
        AppRecord ar2 = appRecImpl.getAppIdSingle(list, 0);

        assertEquals("Objects should be the same", ar, ar2);
    }
    
    /**
     * This will test the delete appointment record method
     *
     * @throws SQLException
     */
    @Test(timeout = 1000)
    public void testDeleteAppRec() throws SQLException {
        AppRecord ar = new AppRecord(51, "test delete", "test", Timestamp.valueOf("2017-12-29 20:45:00"), 
            Timestamp.valueOf("2017-12-30 00:45:00"), "No Details", 0, 3, 1);

        int rec = appRecImpl.deleteAppRec(ar);
        assertEquals("Record deleted should be 1", 1, rec);
    }
    
    /**
     * This will test the update method for the appointment record table
     *
     * @throws SQLException
     */
    @Test(timeout = 2000)
    public void testUpdateAppRec() throws SQLException {
        AppRecord newAppointment = new AppRecord(52, "new test update", "over here",
                Timestamp.valueOf("2017-09-11 12:00:00"), Timestamp.valueOf("2017-09-11 13:00:00"), "details ha", 0, 3, 1);

        int rec = appRecImpl.updateAppRec(newAppointment);
        assertEquals("Record updated should be 1", 1, rec);
    }
    
    /**
     * This will test the find appointment by id method
     *
     * @throws SQLException
     */
    @Test(timeout = 2000)
    public void testFindAppById() throws SQLException {
        AppRecord ar = new AppRecord(50, "Katy Perry", "Centre Bell", Timestamp.valueOf("2017-11-13 20:45:00"),
                Timestamp.valueOf("2017-11-14 00:45:00"), "No details", 0, 3, 1);
        
        AppRecord app = appRecImpl.findAppById(ar);
        assertEquals("The objects should be equal", ar, app);
    }
    
    /**
     * This method will test the find appointment method
     *
     * @throws SQLException
     */
    @Test(timeout = 2000)
    public void testFindAppListForSpecificTimePlus3() throws SQLException {
        Timestamp start = Timestamp.valueOf("2017-09-23 17:07:00");
        Timestamp start2 = Timestamp.valueOf("2017-09-23 17:10:00");

        List<AppRecord> appointments = new ArrayList<AppRecord>();
        appointments = appRecImpl.findAppListForSpecificTimePlus3(start, start2);
        assertEquals("Should have found 3 apppointment record", 
                3, appointments.size());
    }
    
    /**
     * This will test the find appointments between two dates
     *
     * @throws SQLException
     */
    @Test(timeout = 1000)
    public void testFindAppBetweenTwoDates() throws SQLException {
        Timestamp start = Timestamp.valueOf("2017-09-10 00:00:00");
        Timestamp end = Timestamp.valueOf("2017-09-15 23:59:59");

        List<AppRecord> ids = appRecImpl.findAppBetweenTwoDates(start, end);
        int size = ids.size();
        assertEquals("Should find 8 from my test database", 9, size);
    }
    
    /**
     * This will test the find appointments for a day method
     *
     * @throws SQLException
     */
    @Test(timeout = 1000)
    public void testFindAppForDay() throws SQLException {
        String day = "2017-10-06";
        
        List<AppRecord> apps = appRecImpl.findAppForDay(day);
        int size = apps.size();
        assertEquals("Should find 1 appointments in the test database", 1, size);    
    }
    
    /**
     * Test find all appointments for the week
     * @throws SQLException 
     */
    @Test(timeout = 2000)
    public void testFindAppForWeek() throws SQLException {
        String day = "2017-09-12";
        
        List<AppRecord> apps = appRecImpl.findAppForWeek(day);
        int size = apps.size();
        assertEquals("Should find 9 appointments in the test database", 9, size);    
    }
    
    /**
     * This will test the find appointments for a specific month method
     *
     * @throws SQLException
     */
    @Test (timeout = 1000)
    public void testFindAppForMonth() throws SQLException {
        List<AppRecord> list = appRecImpl.findAppForMonth("October");
        int size = list.size();
        
        for (AppRecord ar : list) {
            log.info(ar.toString());
        }
        
        assertEquals("Should find all the appointments for october: 15", 15, size);
    }
    
    /**
     * This will test the get id method
     *
     * @throws SQLException
     */
    @Test(timeout = 2000)
    public void testGetId() throws SQLException {
        AppRecord ar = new AppRecord(1, "Coffee break", "Cafeteria", Timestamp.valueOf("2017-09-11 10:30:00"),
                Timestamp.valueOf("2017-09-11 11:00:00"), "No Details", 0, 1, 0);
        
        List<AppRecord> ids = appRecImpl.getAppFromTitle(ar);

        int size = ids.size();
        
        assertEquals("There are 3 Coffee breaks in the appointments", 3, size);       
    }
    
    /**
     * Should throw an IllegalArgumentException
     *
     * @throws SQLException
     */
    @Test(timeout = 2000, expected = IllegalArgumentException.class)
    public void testInvalidDate() throws SQLException {
        Timestamp start = Timestamp.valueOf("2017-13-13 10:30:00");

        appRecImpl.findApp(start);
        // If an exception was not thrown then the test failed
        fail("The month has a value of 13, which is invalid.  This should"
                + "throw an IllegalArgumentException");
    }
    
    /**
     * This will test the find appointments for a specific month method
     *
     * @throws SQLException
     */
    @Test (timeout = 1000, expected = IllegalArgumentException.class)
    public void testFindAppForInvalidMonth() throws SQLException {
        appRecImpl.findAppForMonth("test");
        
        // If an exception was not thrown then the test failed
        fail("The month has a value of test, which is invalid.  This should"
                + "throw an IllegalArgumentException");
    }
    
    /**
     * This method will test the find appointment method
     *
     * @throws SQLException
     */
    @Test(timeout = 2000)
    public void testIsTimeForReminder() throws SQLException {
        SmtpImpl smtpImpl = new SmtpImpl(config);
        Smtp defaultSmtp = smtpImpl.findDefaultSmtp();
        AppRecord app1 = new AppRecord("alarmtest1", "alarmtest1", Timestamp.valueOf(LocalDateTime.now().plusMinutes(defaultSmtp.getReminder() + 1)),
                Timestamp.valueOf(LocalDateTime.now().plusMinutes(defaultSmtp.getReminder() + 30)), "alarm1", 0, 1, 0);
        AppRecord app2 = new AppRecord("alarmtest2", "alarmtest2", Timestamp.valueOf(LocalDateTime.now().plusMinutes(defaultSmtp.getReminder() + 1)),
                Timestamp.valueOf(LocalDateTime.now().plusMinutes(defaultSmtp.getReminder() + 30)), "alarm2", 0, 1, 0);
        AppRecord app3 = new AppRecord("alarmtest3", "alarmtest3", Timestamp.valueOf(LocalDateTime.now().plusMinutes(defaultSmtp.getReminder() + 1)),
                Timestamp.valueOf(LocalDateTime.now().plusMinutes(defaultSmtp.getReminder() + 30)), "alarm3", 0, 1, 0);

        appRecImpl.createAppRec(app1);
        appRecImpl.createAppRec(app2);
        appRecImpl.createAppRec(app3);
        
        boolean test = appRecImpl.isTimeForReminder();

        assertEquals("Should have found 3 apppointment record = TRUE", 
                true, test);
    }
    
    @AfterClass
    public static void seedAfterTestCompleted() {
        log.info("@AfterClass seeding");
        new AppRecDAOImplTest().seedDatabase();
    }

    /**
     * This routine recreates the database before every test. This makes sure
     * that a destructive test will not interfere with any other test. Does not
     * support stored procedures.
     *
     * This routine is courtesy of Bartosz Majsak, an Arquillian developer at
     * JBoss
     */
    @Before
    public void seedDatabase() {
        log.info("@After seeding");

        final String seedDataScript = loadAsString("CreateJAMTables.sql");
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/jamdb", "kevin", "jampassword");) {
            for (String statement : splitStatements(new StringReader(seedDataScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }

    /**
     * The following methods support the seedDatabse method
     */
    private String loadAsString(final String path) {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
                Scanner scanner = new Scanner(inputStream)) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    private List<String> splitStatements(Reader reader, String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
    }
} // End class
