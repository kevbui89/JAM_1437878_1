package jam.test;

import jam.business.SmtpImpl;
import jam.entities.Configuration;
import jam.entities.Smtp;
import jam.manager.PropertiesManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kevin
 * @version 1.0
 */
public class SmtpDAOImplTest {

    private static final Logger log = LoggerFactory.getLogger(SmtpDAOImplTest.class);
    private Configuration config;
    private SmtpImpl smtpImpl;

    @Before
    public void setUp() throws SQLException, IOException {
        PropertiesManager pm = new PropertiesManager();
        config = new Configuration();
        config = pm.loadTextProperties("src/main/resources", "JamDatabaseProperties", config);

        smtpImpl = new SmtpImpl(config);
    }

    /**
     * This will test if a record is inserted properly in the database with
     * valid data
     *
     * @throws SQLException
     */
    @Test(timeout = 1000)
    public void testInsertSmtp() throws SQLException {
        Smtp s = new Smtp("testobj", "testobj@email.com", "objecttest", "url", 123, 30, 0);
        smtpImpl.createSmtp(s);
        Smtp s2 = smtpImpl.findSmtp("testobj@email.com");

        assertEquals("Row inserted should be 1", s, s2);
    }

    /**
     * Test if find appropriate smtp
     *
     * @throws SQLException
     */
    @Test(timeout = 1000)
    public void testFindSmtp() throws SQLException {
        Smtp s = new Smtp("Jam Test", "jamproject.test123@gmail.com", "jamtest123", "smtp.gmail.com", 465, 120, 1);
        Smtp s2 = smtpImpl.findSmtp("jamproject.test123@gmail.com");

        assertEquals("Did not find the inserted row", s, s2);
    }

    /**
     * This will test the deletion of a smtp record
     *
     * @throws SQLException
     */
    @Test(timeout = 1000)
    public void testDeleteSmtp() throws SQLException {
        Smtp s = new Smtp("Jam Test", "jamproject.test123@gmail.com", "jamtest123", "smtp.gmail.com", 465, 120, 1);
        int rec = smtpImpl.deleteSmtp(s);

        assertEquals("Record deleted should be 1", 1, rec);
    }

    /**
     * This method will test a valid update with null values
     *
     * @throws SQLException
     */
    @Test(timeout = 2000)
    public void testUpdateSmtp() throws SQLException {
        Smtp s = new Smtp("UPDATED", "jamproject.test123@gmail.com", "jamtest123", "smtp.gmail.com", 465, 120, 1);

        int rec = smtpImpl.updateSmtp(s);
        assertEquals("Record updated should be 1 (including null and empty string", 1, rec);
    }

    /**
     * This will test the find default smtp method
     *
     * @throws SQLException
     */
    @Test(timeout = 1000)
    public void testFindDefaultSmtp() throws SQLException {
        Smtp d = new Smtp("Jam Test", "jamproject.test123@gmail.com", "jamtest123",
                "smtp.gmail.com", 465, 120, 1);
        Smtp d2 = smtpImpl.findDefaultSmtp();

        assertEquals("Default smtp record found should be 1, both should be equal", d, d2);
    }

    /**
     * Tests if the new default is set properly
     *
     * @throws SQLException
     */
    @Test(timeout = 2000)
    public void testSetUpNewDefaultSmtp() throws SQLException {
        Smtp newDefault = new Smtp("NEW DEFAULT", "newDEF@gmail.com", "default",
                "defaulturl", 465, 30, 1);
        smtpImpl.createSmtp(newDefault);
        Smtp def = smtpImpl.findDefaultSmtp();

        assertEquals("New default should be updated", newDefault, def);
    }

    /**
     * This will test the update of the old default smtp setting
     *
     * @throws SQLException
     */
    @Test(timeout = 2000)
    public void testSetDefaultSmtp() throws SQLException {
        int rec = smtpImpl.setOldDefaultSmtpToFalse();
        assertEquals("Old default Smtp was unset as default", 1, rec);
    }

    /**
     * Should throw an SQL Exception because the name is longer than 30
     * characters
     *
     * @throws SQLException
     */
    @Test(timeout = 1000, expected = SQLException.class)
    public void testCreateFailureStringLength() throws SQLException {
        Smtp s = new Smtp("TESTINGMORETHAN30CHARACTERSTOCRASHTHISAPPLICATION", "test123@email.com",
                "234234523", "url123", 465, 30, 0);
        smtpImpl.createSmtp(s);

        // If an exception was not thrown then the test failed
        fail("The string that exceeded the length of 30 did not throw the expected exception");
    }

    /**
     * Should throw an SQL Exception because the name is null
     *
     * @throws SQLException
     */
    @Test(timeout = 1000, expected = SQLException.class)
    public void testCreateFailureNullValue() throws SQLException {
        Smtp s = new Smtp(null, "test1234@email.com", "12345656", "url", 465, 30, 0);
        smtpImpl.createSmtp(s);

        // If an exception was not thrown then the test failed
        fail("The string is null, I expect an SQLException");
    }

    @AfterClass
    public static void seedAfterTestCompleted() {
        log.info("@AfterClass seeding");
        new SmtpDAOImplTest().seedDatabase();
    }

    /**
     * This routine recreates the database before every test. This makes sure
     * that a destructive test will not interfere with any other test. Does not
     * support stored procedures.
     *
     * This routine is courtesy of Bartosz Majsak, an Arquillian developer at
     * JBoss
     */
    @Before
    public void seedDatabase() {
        log.info("@After seeding");

        final String seedDataScript = loadAsString("CreateJAMTables.sql");
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/jamdb", "kevin", "jampassword");) {
            for (String statement : splitStatements(new StringReader(seedDataScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }

    /**
     * The following methods support the seedDatabse method
     */
    private String loadAsString(final String path) {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
                Scanner scanner = new Scanner(inputStream)) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    private List<String> splitStatements(Reader reader, String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
    }
} // End test class
