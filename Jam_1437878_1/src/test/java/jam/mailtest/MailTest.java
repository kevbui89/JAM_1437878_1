package jam.mailtest;

import jam.business.AppRecImpl;
import jam.business.SmtpImpl;
import jam.entities.AppRecord;
import jam.entities.Configuration;
import jam.entities.Smtp;
import jam.joddmailsendreceive.Mail;
import jam.test.AppGroupRecDAOImplTest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kevin
 */
public class MailTest {

    private static final Logger log = LoggerFactory.getLogger(MailTest.class);
    private Configuration config;
    private Mail mail;
    private AppRecImpl appRecImpl;
    private SmtpImpl smtpImpl;

    /**
     * Sets up the configuration for the database
     * @throws SQLException 
     */
    @Before
    public void setUp() throws SQLException {

        config = new Configuration();

        config.setUserName("kevin");
        config.setPassword("jampassword");
        config.setUrl("jdbc:mysql://localhost:3306/jamdb");
        this.config = config;

        mail = new Mail(config);
        appRecImpl = new AppRecImpl(config);
        smtpImpl = new SmtpImpl(config);
    }

    /**
     * Creates 3 appointments in 2 hours to send email to
     *
     * @throws SQLException
     */
    public void createAlarmTestAppointments() throws SQLException {
        Smtp defaultSmtp = smtpImpl.findDefaultSmtp();
        AppRecord app1 = new AppRecord("alarmtest1", "alarmtest1", Timestamp.valueOf(LocalDateTime.now().plusMinutes(defaultSmtp.getReminder() + 1)),
                Timestamp.valueOf(LocalDateTime.now().plusMinutes(defaultSmtp.getReminder() + 30)), "alarm1", 0, 1, 0);
        AppRecord app2 = new AppRecord("alarmtest2", "alarmtest2", Timestamp.valueOf(LocalDateTime.now().plusMinutes(defaultSmtp.getReminder() + 1)),
                Timestamp.valueOf(LocalDateTime.now().plusMinutes(defaultSmtp.getReminder() + 30)), "alarm2", 0, 1, 0);
        AppRecord app3 = new AppRecord("alarmtest3", "alarmtest3", Timestamp.valueOf(LocalDateTime.now().plusMinutes(defaultSmtp.getReminder() + 1)),
                Timestamp.valueOf(LocalDateTime.now().plusMinutes(defaultSmtp.getReminder() + 30)), "alarm3", 0, 1, 0);

        appRecImpl.createAppRec(app1);
        appRecImpl.createAppRec(app2);
        appRecImpl.createAppRec(app3);
    }

    /**
     * This will test the send email function
     * @throws SQLException
     */
    @Test(timeout = 60000)
    public void testSendEmail() throws SQLException {
        Smtp defaultSmtp = smtpImpl.findDefaultSmtp();
        LocalDateTime alarmTime = LocalDateTime.now().plusMinutes(defaultSmtp.getReminder());
        LocalDateTime alarmTime2 = LocalDateTime.now().plusMinutes(defaultSmtp.getReminder() + 3);
        Timestamp t = Timestamp.valueOf(alarmTime);
        Timestamp t2 = Timestamp.valueOf(alarmTime2);
        createAlarmTestAppointments();
        List<AppRecord> list = appRecImpl.findAppListForSpecificTimePlus3(t, t2);

        log.debug("List array size is : " + list.size());

        mail.performSendEmail();

        assertEquals("3 emails should be sent", 3, list.size());
    }

    @AfterClass
    public static void seedAfterTestCompleted() {
        log.info("@AfterClass seeding");
        new AppGroupRecDAOImplTest().seedDatabase();
    }

    /**
     * This routine recreates the database before every test. This makes sure
     * that a destructive test will not interfere with any other test. Does not
     * support stored procedures.
     *
     * This routine is courtesy of Bartosz Majsak, an Arquillian developer at
     * JBoss
     */
    @Before
    public void seedDatabase() {
        log.info("@After seeding");

        final String seedDataScript = loadAsString("CreateJAMTables.sql");
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/jamdb", "kevin", "jampassword");) {
            for (String statement : splitStatements(new StringReader(seedDataScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }

    /**
     * The following methods support the seedDatabse method
     */
    private String loadAsString(final String path) {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
                Scanner scanner = new Scanner(inputStream)) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    /**
     * Splits the lines of the SQL file
     * @param reader
     * @param statementDelimiter
     * @return 
     */
    private List<String> splitStatements(Reader reader, String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    /**
     * Checks if the line of the file is a comment
     * @param line  The line to check
     * @return      A boolean if a line is a comment or not
     */
    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
    }
}
