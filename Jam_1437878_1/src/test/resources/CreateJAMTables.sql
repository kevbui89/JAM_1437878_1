USE JAMDB;

DROP TABLE IF EXISTS SMTP_SETTINGS;
DROP TABLE IF EXISTS APPOINTMENTGROUPRECORD;
DROP TABLE IF EXISTS APPOINTMENTRECORD;

-- SMTP Table

CREATE TABLE SMTP_SETTINGS (
UserName varchar(30) NOT NULL,
UserEmail varchar(50) PRIMARY KEY,
UserEmailPassword varchar(50) NOT NULL,
SmtpUrl varchar(50) NOT NULL,
SmtpPortNumber int DEFAULT 465,
Reminder int DEFAULT 15,
DefaultSmtp int DEFAULT 0
);

-- Appointment Group Record Table

CREATE TABLE APPOINTMENTGROUPRECORD (
GroupNumber int NOT NULL AUTO_INCREMENT PRIMARY KEY,
GroupName varchar(30) NOT NULL,
Color varchar(7) NOT NULL
);

-- Appointment Record

CREATE TABLE APPOINTMENTRECORD (
AppointmentID int NOT NULL AUTO_INCREMENT PRIMARY KEY,
Title varchar(30) NOT NULL,
Loc varchar(30) NOT NULL,
StartTime timestamp NOT NULL,
EndTime timestamp NOT NULL,
Details varchar(200) DEFAULT 'No details',
WholeDay int DEFAULT 0,
AppointmentGroup INTEGER,
AlarmReminderReq INT DEFAULT 0
);

-- Inserting data into SMTP_SETTINGS

INSERT INTO SMTP_SETTINGS (UserName, UserEmail, UserEmailPassword, SmtpUrl, SmtpPortNumber, Reminder, DefaultSmtp) VALUES 
('Jam Test', 'jamproject.test123@gmail.com', 'jamtest123', 'smtp.gmail.com', DEFAULT, 120, 1);


-- Inserting data into APPOINTMENTGROUPRECORD

INSERT INTO APPOINTMENTGROUPRECORD (GroupName, Color) VALUES
('Magicians', '#FF0000'),
('Ultimatum', '#0000FF'),
('Purple Power', '#800080'),
('Dawson College', '#ADD8E6'),
('Random Team', '#FFA500');

-- Inserting data into APPOINTMENT RECORD

INSERT INTO APPOINTMENTRECORD (Title, Loc, StartTime, EndTime, Details, WholeDay, AppointmentGroup, AlarmReminderReq) VALUES 
('Coffee break', 'Cafeteria', '2017-09-11 10:30:00', '2017-09-11 11:00:00', DEFAULT, 0, 1, 0),
('Lab Work', 'Lab', '2017-09-17 10:30:00', '2017-09-17 11:30:00', DEFAULT, 0, 1, 0),
('Gym session', 'Gym', '2017-09-12 10:00:00', '2017-09-12 11:00:00', DEFAULT, 0, 1, 0),
('McDonalds Interview', 'McDonalds', '2017-09-14 10:30:00', '2017-09-14 11:00:00', DEFAULT, 0, 1, 0),
('Project Meeting', 'H415', '2017-09-15 10:30:00', '2017-09-15 11:00:00', DEFAULT, 0, 1, 0),
('CSR', 'B1197', '2017-09-10 10:00:00', '2017-09-10 11:00:00', DEFAULT, 0, 2, 0),
('BO Meeting', 'B1197', '2017-09-12 10:00:00', '2017-09-12 11:00:00', DEFAULT, 0, 2, 0),
('Meeting 1', 'A1197', '2017-09-20 16:00:00', '2017-09-20 17:00:00', DEFAULT, 0, 3, 0),
('Meeting 2', 'My house', '2017-09-25 16:00:00', '2017-09-25 17:00:00', DEFAULT, 0, 4, 0),
('Meeting 3', 'School', '2017-09-13 13:00:00', '2017-09-13 14:00:00', DEFAULT, 0, 1, 1),
('Meeting 4', 'School', '2017-09-20 14:00:00', '2017-09-20 14:30:00', DEFAULT, 0, 1, 1),
('Meeting 5', 'B1197', '2017-10-01 08:00:00', '2017-10-01 17:00:00', DEFAULT, 1, 4, 0),
('Final', 'Lab', '2017-11-05 08:00:00', '2017-11-05 17:00:00', DEFAULT, 1, 5, 0),
('Unit Testing', 'Lab', '2017-07-06 12:00:00', '2017-07-06 13:00:00', DEFAULT, 0, 4, 0),
('App Testing', 'Lab', '2017-10-06 13:00:00', '2017-10-06 14:00:00', DEFAULT, 0, 5, 0),
('App Testing 2', 'Lab', '2017-11-10 13:00:00', '2017-11-10 14:00:00', DEFAULT, 0, 1, 1),
('Unit Testing', 'Lab', '2017-12-10 13:00:00', '2017-12-10 14:00:00', DEFAULT, 0, 1, 1),
('Coffee Break', 'Cafeteria', '2017-12-12 10:00:00', '2017-12-12 11:00:00', DEFAULT, 0, 2, 0),
('Database Discussion', 'Lab', '2017-11-25 13:00:00', '2017-11-25 14:00:00', DEFAULT, 0, 2, 0),
('Briefing', 'Lab', '2017-09-25 13:00:00', '2017-09-25 14:00:00', DEFAULT, 0, 3, 1),
('Brainstorm Project', 'My house', '2017-10-16 09:00:00', '2017-10-16 10:00:00', DEFAULT, 0, 4, 1),
('Objective', 'School', '2017-10-11 08:00:00', '2017-10-11 17:00:00', DEFAULT, 1, 1, 0),
('Shopping', 'Galeries Danjou', '2017-12-05 08:00:00', '2017-12-05 17:00:00', DEFAULT, 1, 1, 1),
('Study Session', 'Starbucks', '2017-10-26 12:00:00', '2017-10-26 16:00:00', DEFAULT, 0, 2, 1),
('Coffee Break', 'Cafeteria', '2017-08-11 11:00:00', '2017-08-11 11:30:00', DEFAULT, 0, 5, 0),
('Tutorial', 'Library', '2017-09-12 08:00:00', '2017-09-12 09:00:00', DEFAULT, 0, 5, 0),
('Tutorial', 'Library', '2017-09-11 10:45:00', '2017-09-11 11:15:00', DEFAULT, 0, 5, 0),
('Dentist Appointment', 'Dentist Office', '2017-10-12 09:45:00', '2017-10-12 10:45:00', DEFAULT, 0, 2, 1),
('Study Session', 'Library', '2017-12-11 13:45:00', '2017-12-11 16:30:00', DEFAULT, 0, 1, 1),
('Random Meeting', 'Library', '2017-09-17 12:45:00', '2017-09-17 13:30:00', DEFAULT, 0, 4, 0),
('Another Random Meeting', 'Starbucks', '2017-11-11 9:45:00', '2017-11-11 16:30:00', DEFAULT, 1, 5, 0),
('Coffee', 'Tim Hortons', '2017-11-15 18:45:00', '2017-11-15 19:30:00', DEFAULT, 0, 5, 1),
('Supper', 'Ottavio', '2017-10-11 19:30:00', '2017-10-11 22:30:00', DEFAULT, 0, 5, 0),
('Dentist Appointment', 'Dentist Office', '2017-10-22 09:45:00', '2017-10-22 10:45:00', DEFAULT, 0, 2, 1),
('Doctor Appointment', 'Doctor Office', '2017-09-18 07:45:00', '2017-09-18 08:45:00', DEFAULT, 0, 3, 1),
('Water Slides', 'Water Park', '2017-09-02 08:45:00', '2017-09-02 17:30:00', DEFAULT, 1, 3, 0),
('My birthday', 'Club', '2017-10-03 18:45:00', '2017-10-03 23:30:00', DEFAULT, 0, 4, 0),
('Strawberry Picking', 'Forest', '2017-10-02 08:45:00', '2017-10-02 17:30:00', DEFAULT, 1, 2, 0),
('Training', 'Gym', '2017-10-20 08:45:00', '2017-10-20 09:45:00', DEFAULT, 0, 1, 1),
('Training 2', 'Gym', '2017-10-22 08:45:00', '2017-10-22 09:45:00', DEFAULT, 0, 1, 1),
('Training 3', 'Gym', '2017-10-24 08:45:00', '2017-10-24 09:45:00', DEFAULT, 0, 5, 1),
('Spartan Race', 'Gym', '2017-10-27 08:45:00', '2017-10-27 17:45:00', DEFAULT, 1, 5, 0),
('Interior Design', 'House', '2017-11-02 08:45:00', '2017-11-02 12:45:00', DEFAULT, 0, 1, 0),
('Boxing Match', 'MGM', '2017-12-28 20:45:00', '2017-12-28 00:45:00', DEFAULT, 0, 5, 1),
('Tutoring', 'School', '2017-11-28 19:45:00', '2017-11-28 20:45:00', DEFAULT, 0, 2, 1),
('Dreamhack', 'Place Bonaventure', '2017-10-28 08:45:00', '2017-10-28 21:45:00', DEFAULT, 1, 2, 0),
('Funerals', 'Funeral House', '2017-12-25 20:45:00', '2017-12-25 21:45:00', DEFAULT, 0, 2, 1),
('Metallica', 'Metropolis', '2017-09-27 20:45:00', '2017-09-28 00:45:00', DEFAULT, 0, 4, 1),
('AC/DC', 'Centre Bell', '2017-11-23 20:45:00', '2017-11-24 00:45:00', DEFAULT, 0, 2, 1),
('Katy Perry', 'Centre Bell', '2017-11-13 20:45:00', '2017-11-14 00:45:00', DEFAULT, 0, 3, 1),
('test delete', 'test', '2017-12-29 20:45:00', '2017-12-30 00:45:00', DEFAULT, 0, 3, 1),
('test update', 'test', '2017-01-29 20:45:00', '2017-01-30 00:45:00', DEFAULT, 0, 3, 1),
('3appoints test', 'Cafeteria', '2017-09-23 17:08:00', '2017-09-23 23:59:00', DEFAULT, 0, 1, 0),
('3appoints test', 'Lab', '2017-09-23 17:08:00', '2017-09-23 23:59:00', DEFAULT, 0, 1, 0),
('3appoints test', 'Gym', '2017-09-23 17:08:00', '2017-09-23 23:59:00', DEFAULT, 0, 1, 0),
('3appoints test', 'Cafeteria', '2017-09-28 20:16:00', '2017-09-28 23:59:00', DEFAULT, 0, 1, 0),
('3appoints test', 'Lab', '2017-09-28 20:16:00', '2017-09-28 23:59:00', DEFAULT, 0, 1, 0),
('3appoints test', 'Gym', '2017-09-28 20:16:00', '2017-09-28 23:59:00', DEFAULT, 0, 1, 0);
