package jam.entities;

import java.util.Objects;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Kevin
 */
public class Configuration {
    
    private StringProperty userName;
    private StringProperty password;
    private StringProperty url;
    
    public Configuration() {
        this("", "", "");
    }
    
    public Configuration(String username, String password, String url) {
        super();
        this.userName = new SimpleStringProperty(username);
        this.password = new SimpleStringProperty(password);
        this.url = new SimpleStringProperty(url);
    }
    
    public String getUserName() {
        return userName.get();
    }

    public String getPassword() {
        return password.get();
    }

    public String getUrl() {
        return url.get();
    }

    public void setUserName(String un) {
        userName.set(un);
    }

    public void setPassword(String pw) {
        password.set(pw);
    }

    public void setUrl(String u) {
        url.set(u);
    }

    public StringProperty userNameProperty() {
        return userName;
    }

    public StringProperty passwordProperty() {
        return password;
    }

    public StringProperty urlProperty() {
        return url;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.userName.get());
        hash = 17 * hash + Objects.hashCode(this.password.get());
        hash = 17 * hash + Objects.hashCode(this.url.get());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Configuration other = (Configuration) obj;
        if (!Objects.equals(this.userName.get(), other.userName.get())) {
            return false;
        }
        if (!Objects.equals(this.password.get(), other.password.get())) {
            return false;
        }
        return Objects.equals(this.url.get(), other.url.get());
    }

    @Override
    public String toString() {
        return "Configuration\n{\t" + "userName=" + userName.get() 
                + "\n\tpassword=" + password.get() + "\n\turl=" + url.get() + "}";
    }
}
