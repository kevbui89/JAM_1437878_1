package jam.entities;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Kevin
 */
public class WeeklyAppointmentRowBean {
    
    private StringProperty dateTime;
    private StringProperty sunday;
    private StringProperty monday;
    private StringProperty tuesday;
    private StringProperty wednesday;
    private StringProperty thursday;
    private StringProperty friday;
    private StringProperty saturday;
    
    public WeeklyAppointmentRowBean(String dt, String sunday, String monday, String tuesday, 
            String wednesday, String thursday, String friday, String saturday) {
        this.dateTime = new SimpleStringProperty(dt);
        this.sunday = new SimpleStringProperty(sunday);
        this.monday = new SimpleStringProperty(monday);
        this.tuesday = new SimpleStringProperty(tuesday);
        this.wednesday = new SimpleStringProperty(wednesday);
        this.thursday = new SimpleStringProperty(thursday);
        this.friday = new SimpleStringProperty(friday);
        this.saturday = new SimpleStringProperty(saturday);
    }
    
    public WeeklyAppointmentRowBean() {
        this("", "", "", "", "", "", "", "");
    }
    
    public void setDateTime(String dt) {
        dateTime.set(dt);
    }
    
    public String getDateTime() {
        return dateTime.get();
    }
    
    public StringProperty dateTimeProperty() {
        return dateTime;
    }
    
    public String getMonday() {
        return monday.get();
    }
    
    public void setMonday(String m) {
        this.monday.set(m);
    }
    
    public StringProperty mondayProperty() {
        return monday;
    }
        
    public String getSunday() {
        return sunday.get();
    }
    
    public void setSunday(String s) {
        this.sunday.set(s);
    }
    
    public StringProperty sundayProperty() {
        return sunday;
    }
    
    public String getTuesday() {
        return tuesday.get();
    }
    
    public void setTuesday(String t) {
        this.tuesday.set(t);
    }
    
    public StringProperty tuesdayProperty() {
        return tuesday;
    }
    
    public String getWednesday() {
        return wednesday.get();
    }
    
    public void setWednesday(String w) {
        this.wednesday.set(w);
    }
    
    public StringProperty wednesdayProperty() {
        return wednesday;
    }
    
    public String getThursday() {
        return thursday.get();
    }
    
    public void setThursday(String t) {
        this.thursday.set(t);
    }
    
    public StringProperty thursdayProperty() {
        return thursday;
    }
    
    public String getFriday() {
        return friday.get();
    }
    
    public void setFriday(String f) {
        this.friday.set(f);
    }
    
    public StringProperty fridayProperty() {
        return friday;
    }
    
    public String getSaturday() {
        return saturday.get();
    }
    
    public void setSaturday(String s) {
        this.saturday.set(s);
    }
    
    public StringProperty saturdayProperty() {
        return saturday;
    }
    
    /**
     * Always going to be different depending on the appointments per week
     * @return 
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((sunday.get() == null) ? 0 : sunday.get().hashCode());
        result = prime * result
                + ((monday.get() == null) ? 0 : monday.get().hashCode());
        result = prime * result
                + ((tuesday.get() == null) ? 0 : tuesday.get().hashCode());
        result = prime * result
                + ((wednesday.get() == null) ? 0 : wednesday.get().hashCode());
        result = prime * result
                + ((thursday.get() == null) ? 0 : thursday.get().hashCode());
        result = prime * result
                + ((friday.get() == null) ? 0 : friday.get().hashCode());
        result = prime * result
                + ((saturday.get() == null) ? 0 : saturday.get().hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return true;
    }
    
    @Override
    public String toString() {
        return "AppBean{datetime=" + dateTime.get() + "}";
    }
}