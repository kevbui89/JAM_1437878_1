package jam.entities;

import java.util.Objects;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * The smtp object class
 * 
 * @author Kevin
 */
public class Smtp {

    private StringProperty username;
    private StringProperty email;
    private StringProperty password;
    private StringProperty url;
    private IntegerProperty portNo;
    private IntegerProperty reminder;
    private IntegerProperty defaultSmtp;

    /**
     * Default constructor
     */
    public Smtp() {
        this("", "", "", "", 0, 0, 0);
    }

    /**
     * Seven parameter constructor
     * @param username  The username
     * @param email     The email
     * @param password  The password
     * @param url       The url
     * @param portNo    The port number
     * @param reminder  The reminder
     * @param defaultSmtp   The default Smtp
     */
    public Smtp(String username, String email, String password, String url, int portNo, int reminder, int defaultSmtp) {
        this.username = new SimpleStringProperty(username);
        this.email = new SimpleStringProperty(email);
        this.password = new SimpleStringProperty(password);
        this.url = new SimpleStringProperty(url);
        this.portNo = new SimpleIntegerProperty(portNo);
        this.reminder = new SimpleIntegerProperty(reminder);
        this.defaultSmtp = new SimpleIntegerProperty(defaultSmtp);
    }

    public String getUsername() {
        return username.get();
    }

    public void setUsername(String username) {
        this.username.set(username);
    }

    public StringProperty userNameProperty() {
        return username;
    }

    public String getEmail() {
        return email.get();
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    public StringProperty emailProperty() {
        return email;
    }

    public String getPassword() {
        return password.get();
    }

    public void setPassword(String password) {
        this.password.set(password);
    }

    public StringProperty passwordProperty() {
        return password;
    }

    public String getUrl() {
        return url.get();
    }

    public void setUrl(String url) {
        this.url.set(url);
    }

    public StringProperty urlProperty() {
        return url;
    }

    public int getPortNumber() {
        return portNo.get();
    }

    public void setPortNumber(int pn) {
        this.portNo.set(pn);
    }

    public IntegerProperty portNumberProperty() {
        return portNo;
    }

    public int getReminder() {
        return reminder.get();
    }

    public void setReminder(int r) {
        this.reminder.set(r);
    }

    public IntegerProperty reminderProperty() {
        return reminder;
    }

    public int getDefaultSmtp() {
        return defaultSmtp.get();
    }

    public void setDefaultSmtp(int dsmtp) {
        this.defaultSmtp.set(dsmtp);
    }

    public IntegerProperty defaultSmtpProperty() {
        return defaultSmtp;
    }

    @Override
    public int hashCode() {
        int hash = 9;
        hash = 21 * hash + Objects.hashCode(this.email);

        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        Smtp other = (Smtp) obj;
        if (username.get() == null) {
            if (other.username.get() != null) {
                return false;
            }
        } else if (!username.get().equals(other.username.get())) {
            return false;
        }

        if (email.get() == null) {
            if (other.email.get() != null) {
                return false;
            }
        } else if (!email.get().equals(other.email.get())) {
            return false;
        }

        if (password.get() == null) {
            if (other.password.get() != null) {
                return false;
            }
        } else if (!password.get().equals(other.password.get())) {
            return false;
        }

        if (url.get() == null) {
            if (other.url.get() != null) {
                return false;
            }
        } else if (!url.get().equals(other.url.get())) {
            return false;
        }
        
        if (portNo.get() != other.portNo.get())
            return false;
        
        if (reminder.get() != other.reminder.get())
            return false;
        
        if (defaultSmtp.get() != other.defaultSmtp.get())
            return false;

        return true;
    }

    @Override
    public String toString() {
        return "SMTP{username=" + username.get() + ", email=" + email.get() + ", password="
                + password.get() + ", url=" + url.get() + ", portnumber=" 
                + portNo.get() + ", reminder=" + reminder.get() + ", defaultsmtp=" 
                + defaultSmtp.get() + "}";
    }
}
