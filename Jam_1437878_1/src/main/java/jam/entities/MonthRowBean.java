package jam.entities;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Object to display the numbers on the month
 * This might get changed into the AppRecord bean to display with titles and 
 * start time
 * 
 * @author Kevin
 */
public class MonthRowBean {
    
    private StringProperty col1;
    private StringProperty col2;
    private StringProperty col3;
    private StringProperty col4;
    private StringProperty col5;
    private StringProperty col6;
    private StringProperty col7;
    private ObjectProperty<AppRecord> appointment;
    
    public MonthRowBean() {
            this("", "", "", "", "", "", "", null);
    }
    
    public MonthRowBean(String col1, String col2, String col3, String col4, String col5, String col6, String col7, AppRecord app) {
        this.col1 = new SimpleStringProperty(col1);
        this.col2 = new SimpleStringProperty(col2);
        this.col3 = new SimpleStringProperty(col3);
        this.col4 = new SimpleStringProperty(col4);
        this.col5 = new SimpleStringProperty(col5);
        this.col6 = new SimpleStringProperty(col6);
        this.col7 = new SimpleStringProperty(col7);
        this.appointment = new SimpleObjectProperty<>(app);
    }
    
    public void setCol1(String data) {
        col1.set(data);
    }
    
    public String getCol1() {
        return col1.get();
    }
    
    public StringProperty col1Property() {
        return col1;
    }
    
    public void setCol2(String data) {
        col2.set(data);
    }
    
    public String getCol2() {
        return col2.get();
    }
    
    public StringProperty col2Property() {
        return col2;
    }
    
    public void setCol3(String data) {
        col3.set(data);
    }
    
    public String getCol3() {
        return col3.get();
    }
    
    public StringProperty col3Property() {
        return col3;
    }
    
    public void setCol4(String data) {
        col4.set(data);
    }
    
    public String getCol4() {
        return col4.get();
    }
    
    public StringProperty col4Property() {
        return col4;
    }
    
    public void setCol5(String data) {
        col5.set(data);
    }
    
    public String getCol5() {
        return col5.get();
    }
    
    public StringProperty col5Property() {
        return col5;
    }
    
    public void setCol6(String data) {
        col6.set(data);
    }
    
    public String getCol6() {
        return col6.get();
    }
    
    public StringProperty col6Property() {
        return col6;
    }
    
    public void setCol7(String data) {
        col7.set(data);
    }
    
    public String getCol7() {
        return col7.get();
    }
    
    public StringProperty col7Property() {
        return col7;
    }
    
    public void setAppointment(AppRecord data) {
        appointment.set(data);
    }
    
    public AppRecord getAppointment() {
        return appointment.get();
    }
    
    public ObjectProperty appointmentProperty() {
        return appointment;
    }

    @Override
    public String toString() {
        return "MonthRowBean{" + "col1=" + col1 + ", col2=" + col2 + ", col3=" 
                + col3 + ", col4=" + col4 + ", col5=" + col5 + ", col6=" 
                + col6 + ", col7=" + col7 + ", appointment=" + appointment + "}";
    }   
}
