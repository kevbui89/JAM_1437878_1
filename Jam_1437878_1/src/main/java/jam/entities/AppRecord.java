package jam.entities;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Kevin
 */
public class AppRecord {
    
    private IntegerProperty appId;
    private StringProperty title;
    private StringProperty location;
    private ObjectProperty<Timestamp> start;
    private ObjectProperty<LocalDate> startDate;
    private StringProperty startTime;
    private ObjectProperty<Timestamp> end;
    private ObjectProperty<LocalDate> endDate;
    private StringProperty endTime;
    private StringProperty details;
    private IntegerProperty wholeDay;
    private IntegerProperty appGroup;
    private IntegerProperty alarm;
    
    public AppRecord(int appId, String title, String location, Timestamp start,
            Timestamp end, String details, int wholeDay, int appGroup, int alarm) {
        this.appId = new SimpleIntegerProperty(appId);
        this.title = new SimpleStringProperty(title);
        this.location = new SimpleStringProperty(location);
        this.start = new SimpleObjectProperty<>(start);
        this.end = new SimpleObjectProperty<>(end);
        this.details = new SimpleStringProperty(details);
        this.wholeDay = new SimpleIntegerProperty(wholeDay);
        this.appGroup = new SimpleIntegerProperty(appGroup);
        this.alarm = new SimpleIntegerProperty(alarm);
    }
    
    public AppRecord(String title, String location, Timestamp start,
            Timestamp end, String details, int wholeDay, int appGroup, int alarm) {
        this.title = new SimpleStringProperty(title);
        this.location = new SimpleStringProperty(location);
        this.start = new SimpleObjectProperty<>(start);
        this.end = new SimpleObjectProperty<>(end);
        this.details = new SimpleStringProperty(details);
        this.wholeDay = new SimpleIntegerProperty(wholeDay);
        this.appGroup = new SimpleIntegerProperty(appGroup);
        this.alarm = new SimpleIntegerProperty(alarm);
    }
    
    public AppRecord() {
        this(0, "", "", null, null, "", 0, 0, 0);
    }
    
    public int getAppId() {
        return appId.get();
    }

    public void setAppId(int id) {
        this.appId.set(id);
    }
    
    public IntegerProperty idProperty() {
        return appId;
    }
    
    public String getTitle() {
        return title.get();
    }
    
    public void setTitle(String title) {
        this.title.set(title);
    }
    
    public StringProperty titleProperty() {
        return title;
    }
    
    public String getLocation() {
        return location.get();
    }
    
    public void setLocation(String loc) {
        this.location.set(loc);
    }
    
    public StringProperty locationProperty() {
        return location;
    }
    
    public ObjectProperty<LocalDate> dateStartFieldProperty() {
        LocalDate s = start.get().toLocalDateTime().toLocalDate();
        this.startDate = new SimpleObjectProperty<>(s);
        return startDate;
    }
    
    public Timestamp getStartTimeStampValue() {
        return Timestamp.valueOf(start.get().toString());
    }

    public void setStartTimeStampValue(Timestamp ts) {
        start.set(ts);
    }
    
    public Timestamp getStartTime() {
        return start.get();
    }
    
    public StringProperty StartTimeFormProperty() {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String s = sdf.format(Timestamp.valueOf(start.get().toString()));
        this.startTime = new SimpleStringProperty(s);
        return startTime;
    }    
    
    public void setStartTime(Timestamp start) {
        this.start.set(start);
    }
    
    public ObjectProperty startTimeProperty() {
        return start;
    }
    
    public ObjectProperty<LocalDate> dateEndFieldProperty() {
        LocalDate s = end.get().toLocalDateTime().toLocalDate();
        this.endDate = new SimpleObjectProperty<>(s);
        return endDate;
    }
    
    public Timestamp getEndTimeStampValue() {
        return Timestamp.valueOf(end.get().toString());
    }

    public void setEndTimeStampValue(Timestamp ts) {
        end.set(ts);
    }
    
    public Timestamp getEndTime() {
        return end.get();
    }
    
    public StringProperty EndTimeFormProperty() {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String s = sdf.format(Timestamp.valueOf(end.get().toString()));
        this.endTime = new SimpleStringProperty(s);
        return endTime;
    }
    
    public void setEndTime(Timestamp end) {
        this.end.set(end);
    }
    
    public ObjectProperty endTimeProperty() {
        return end;
    }
    
    public String getDetails() {
        return details.get();
    }
    
    public void setDetails(String details) {
        this.details.set(details);
    }
    
    public StringProperty detailsProperty() {
        return details;
    }
    
    public int getWholeDay() {
        return wholeDay.get();
    }
    
    public void setWholeDay(int wd) {
        this.wholeDay.set(wd);
    }
    
    public IntegerProperty wholeDayProperty() {
        return wholeDay;
    }
    
    public int getAppGroup() {
        return appGroup.get();
    }
    
    public void SetAppGroup(int ag) {
        this.appGroup.set(ag);
    }
    
    public IntegerProperty groupProperty() {
        return appGroup;
    }
    
    public int getAlarm() {
        return alarm.get();
    }
    
    public void setAlarm(int alarm) {
        this.alarm.set(alarm);
    }
    
    public IntegerProperty alarmProperty() {
        return alarm;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((title.get() == null) ? 0 : title.get().hashCode());
        result = prime * result
                + ((location.get() == null) ? 0 : location.get().hashCode());
        result = prime * result
                + ((start.get() == null) ? 0 : start.get().hashCode());
        result = prime * result
                + ((end.get() == null) ? 0 : end.get().hashCode());
        result = prime * result
                + ((details.get() == null) ? 0 : details.get().hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AppRecord other = (AppRecord) obj;
        if (title.get() == null) {
            if (other.title.get() != null)
                return false;
        } else if (!title.get().equals(other.title.get()))
            return false;
        if (location.get() == null) {
            if (other.location.get() != null)
                return false;
        } else if (!location.get().equals(other.location.get()))
            return false;
        if (start.get() == null) {
            if (other.start.get() != null)
                return false;
        } else if (!start.get().equals(other.start.get()))
            return false;
        if (end.get() == null) {
            if (other.end.get() != null)
                return false;
        } else if (!end.get().equals(other.end.get()))
            return false;
        if (details.get() == null) {
            if (other.details.get() != null)
                return false;
        } else if (!details.get().equals(other.details.get()))
            return false;

        return true;
    }
    
    @Override
    public String toString() {
        return "AppBean{id=" + appId.get() 
                + ", title=" + title.get() + ", location=" + location.get() 
                + ", start=" + start.get() + ", end=" + end.get() + ", details=" 
                + details.get() + ", wholeday=" + wholeDay.get() + ", group=" 
                + appGroup.get() + ",alarm=" + alarm.get() + '}';
    }
}
