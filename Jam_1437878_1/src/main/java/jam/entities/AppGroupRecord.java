package jam.entities;

import java.util.Objects;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * The appointment group record class
 * @author Kevin
 */
public class AppGroupRecord {
    
    private IntegerProperty groupNumber;
    private StringProperty groupName;
    private StringProperty color;
    
    public AppGroupRecord(){
        this(0, "", "");
    }
    
    public AppGroupRecord(int groupNumber, String groupName, String color) {
        this.groupNumber = new SimpleIntegerProperty(groupNumber);
        this.groupName = new SimpleStringProperty(groupName);
        this.color = new SimpleStringProperty(color);
    }

    public int getId() {
        return groupNumber.get();
    }
    
    public void setId(int id) {
        this.groupNumber.set(id);
    }
    
    public IntegerProperty idProperty() {
        return groupNumber;
    }
    
    public String getGroupName() {
        return groupName.get();
    }
    
    public void setGroupName(String gn) {
        this.groupName.set(gn);
    }
    
    public StringProperty groupNameProperty() {
        return groupName;
    }
    
    public String getColor() {
        return color.get();
    }
    
    public void setColor (String color) {        
        this.color.set(color);
    }
    
    public StringProperty colorProperty() {
        return color;
    }
    
    @Override
    public int hashCode() {
        int hash = 9;
        hash = 21 * hash + Objects.hashCode(this.groupNumber);
        
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        
        AppGroupRecord other = (AppGroupRecord) obj;
        if (groupName.get() == null) {
            if (other.groupName.get() != null) {
                return false;
            }
        } else if (!groupName.get().equals(other.groupName.get())) {
            return false;
        }

        if (color.get() == null) {
            if (other.color.get() != null) {
                return false;
            }
        } else if (!color.get().equals(other.color.get())) {
            return false;
        }
        
        return true;
    }
    
    @Override
    public String toString() {
        return groupNumber + "/" + groupName + "/" + color;
    }
}
