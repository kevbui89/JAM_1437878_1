package jam.entities;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Kevin
 */
public class DailyAppointmentRowBean {
    
    private StringProperty dateTime;
    private IntegerProperty id;
    private StringProperty title;
    
    public DailyAppointmentRowBean(String dt, String title) {
        this.dateTime = new SimpleStringProperty(dt);
        this.title = new SimpleStringProperty(title);
    }
    
    public DailyAppointmentRowBean() {
        this("", "");
    }
    
    public void setDateTime(String dt) {
        dateTime.set(dt);
    }
    
    public String getDateTime() {
        return dateTime.get();
    }
    
    public StringProperty dateTimeProperty() {
        return dateTime;
    }
    
    public int getId() {
        return id.get();
    }
    
    public void setId(int id) {
        this.id.set(id);
    }
    
    public IntegerProperty idProperty() {
        return id;
    }
    
    public String getTitle() {
        return title.get();
    }
    
    public void setTitle(String title) {
        this.title.set(title);
    }
    
    public StringProperty titleProperty() {
        return title;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((title.get() == null) ? 0 : title.get().hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DailyAppointmentRowBean other = (DailyAppointmentRowBean) obj;
        if (title.get() == null) {
            if (other.title.get() != null)
                return false;
        } else if (!title.get().equals(other.title.get()))
            return false;

        return true;
    }
    
    @Override
    public String toString() {
        return "AppBean{datetime=" + dateTime.get() + ", title=" + title.get() + "}";
    }
}