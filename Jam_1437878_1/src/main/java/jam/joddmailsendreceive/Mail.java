package jam.joddmailsendreceive;

import jam.business.AppRecImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import jodd.mail.Email;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;
import jodd.mail.SmtpSslServer;
import jam.business.SmtpImpl;
import jam.entities.AppRecord;
import jam.entities.Configuration;
import jam.entities.Smtp;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

/**
 * The mail class takes care of the sending of the emails
 * @author Kevin
 */
public class Mail {

    private final Logger log = LoggerFactory.getLogger(getClass().getName());
    private String smtpServerName;
    private String emailSend;
    private String emailSendPwd;
    private String emailReceive;
    private String emailReceivePwd;
    private Configuration config;

    /**
     * Constructor
     *
     * @param config The configuration
     * @throws SQLException
     */
    public Mail(Configuration config) throws SQLException {
        this.config = config;
    }

    /**
     * This method is where the different uses of Jodd are exercised
     */
    public void performSendEmail() throws SQLException {
        AppRecImpl ari = new AppRecImpl(config);
        SmtpImpl smtpImpl = new SmtpImpl(config);

        if (ari.isTimeForReminder()) {
            setUp();
            Smtp defaultSmtp = smtpImpl.findDefaultSmtp();
            LocalDateTime alarmTime = LocalDateTime.now().plusMinutes(defaultSmtp.getReminder());
            LocalDateTime alarmTime2 = LocalDateTime.now().plusMinutes(defaultSmtp.getReminder() + 3);
            Timestamp t = Timestamp.valueOf(alarmTime);
            Timestamp t2 = Timestamp.valueOf(alarmTime2);
            List<AppRecord> list = ari.findAppListForSpecificTimePlus3(t, t2);
            if (!list.isEmpty()) {
                for (AppRecord ar : list) {
                    sendEmail(ar);
                }
            } else {
                log.info("There were no emails to send");
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                log.error("Threaded sleep failed", e);
                System.exit(1);
            }
        } // End if 
    } // End performSendEmail

    /**
     * Standard send routine using Jodd. Jodd knows about GMail so no need to
     * include port information
     * @param ar    The appointment record that contains the info to be sent
     */
    public void sendEmail(AppRecord ar) {
        log.debug("Inside sendEmail");
        log.debug(smtpServerName);
        log.debug(emailSend);
        log.debug(emailSendPwd);
        SmtpServer<SmtpSslServer> smtpServer = SmtpSslServer
                .create(smtpServerName)
                .authenticateWith(emailSend, emailSendPwd);

        smtpServer.debug(true);

        Email email = Email.create().from(emailSend)
                .to(emailReceive)
                .subject("Appointment Reminder").addText(ar.getTitle() + " at "
                + ar.getLocation() + " at " + ar.getStartTime().toString());

        SendMailSession session = smtpServer.createSession();

        session.open();
        session.sendMail(email);
        session.close();
        log.debug("Email has been sent: " + ar.getTitle());
    }

    /**
     * Sets up the info of the sender and receiver
     * @throws SQLException 
     */
    private void setUp() throws SQLException {
        SmtpImpl s = new SmtpImpl(config);
        Smtp defaultSmtp = s.findDefaultSmtp();
        smtpServerName = defaultSmtp.getUrl();
        emailSend = defaultSmtp.getEmail();
        emailSendPwd = defaultSmtp.getPassword();
        emailReceive = defaultSmtp.getEmail();
        emailReceivePwd = defaultSmtp.getPassword();
        log.debug("Email Send & receive is set: " + emailSend);
    }
} // End class
