package jam.joddmailsendreceive;

import jam.business.AppRecImpl;
import jam.entities.Configuration;
import jam.manager.PropertiesManager;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kevin
 */
public class EmailTask implements Runnable {

    private final Logger log = LoggerFactory.getLogger(this.getClass()
            .getName());
    private final Mail emailSender;
    private Configuration config;
    private AppRecImpl ari;

    /**
     * Constructor creates an instance of the thread safe EmailSender
     */
    public EmailTask() throws SQLException, IOException {
        setUp();
        emailSender = new Mail(config);
        ari = new AppRecImpl(config);
    }

    /**
     * Sets up the Configuration
     */
    public void setUp() throws IOException {
        PropertiesManager pm = new PropertiesManager();
        config = new Configuration();
        config = pm.loadTextProperties("src/main/resources", "JamDatabaseProperties", config);
    }

    /**
     * This method is called by the executor when the defined interval ends
     */
    @Override
    public void run() {
        Platform.runLater(() -> {
            try {
                if (ari.isTimeForReminder()) {
                    emailSender.performSendEmail();

                    log.info("Sent an email");

                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Sending An Email");
                    alert.setHeaderText("Sending An Email.");
                    alert.setContentText("An email has been sent.");
                    alert.showAndWait();
                }
            } catch (SQLException ex) {
                java.util.logging.Logger.getLogger(EmailTask.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }
}
