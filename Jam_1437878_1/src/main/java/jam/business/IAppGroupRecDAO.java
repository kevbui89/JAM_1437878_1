package jam.business;

import jam.entities.AppGroupRecord;
import java.sql.SQLException;
import java.util.List;

/**
 * The interface for the appointment group record
 * @author Kevin
 */
public interface IAppGroupRecDAO {
    
    // Create
    public int createAppGroupRec(AppGroupRecord agr) throws SQLException;
    
    // Delete
    public int deleteAppGroupRec(AppGroupRecord agr) throws SQLException;
    
    // Update
    public int updateAppGroupRec(AppGroupRecord agr) throws SQLException;
    
    //Read
    public AppGroupRecord findAppGroupRec(AppGroupRecord agr) throws SQLException;
    
    public List<AppGroupRecord> findAppGroupRec() throws SQLException;
    
    
}
