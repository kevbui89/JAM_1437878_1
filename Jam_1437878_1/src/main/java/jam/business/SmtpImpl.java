package jam.business;

import jam.entities.Configuration;
import jam.entities.Smtp;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements the ISmtpDAO methods that are used to fetch and manipulate
 * the data from the database
 * @author Kevin
 */
public class SmtpImpl implements ISmtpDAO {

    private Configuration config;
    private String url;
    private String username;
    private String password;
    private final Logger log = LoggerFactory.getLogger(this.getClass().getName());

    public SmtpImpl() {
        super();
    }

    public SmtpImpl(Configuration config) {
        this.config = config;
        this.username = config.getUserName();
        this.password = config.getPassword();
        this.url = config.getUrl();
    }

    /**
     * Creates an smtp user
     * @param smtp  The smtp to create
     * @throws SQLException
     */
    public int createSmtp(Smtp smtp) throws SQLException {
        String query = "INSERT INTO SMTP_SETTINGS (UserName, UserEmail, UserEmailPassword, SmtpUrl, SmtpPortNumber, Reminder, DefaultSmtp) VALUES (?,?,?,?,?,?,?)";
        int records;

        try (Connection connection = DriverManager.getConnection(url, username, password);
                PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);) {

            if (smtp.getDefaultSmtp() == 1) {
                setOldDefaultSmtpToFalse();
            }

            ps.setString(1, smtp.getUsername());
            ps.setString(2, smtp.getEmail());
            ps.setString(3, smtp.getPassword());
            ps.setString(4, smtp.getUrl());
            ps.setInt(5, smtp.getPortNumber());
            ps.setInt(6, smtp.getReminder());
            ps.setInt(7, smtp.getDefaultSmtp());

            records = ps.executeUpdate();
            try (ResultSet rs = ps.getGeneratedKeys()) {
                String uName = "";
                if (rs.next()) {
                    uName = rs.getString(1);
                }

                log.debug("New record User Name is " + uName);
            }
        } catch (SQLException ex) {
            log.debug("Exception: " + ex);
            throw ex;
        }

        return records;
    }

    /**
     * Deletes an smtp record
     * @param smtp The smtp to delete
     * @throws SQLException
     */
    public int deleteSmtp(Smtp smtp) throws SQLException {
        String query = "DELETE FROM SMTP_SETTINGS WHERE UserEmail = ?";
        int record;

        try (Connection connection = DriverManager.getConnection(url, username, password);
                PreparedStatement deleteSmtp = connection.prepareStatement(query);) {

            deleteSmtp.setString(1, smtp.getEmail());

            record = deleteSmtp.executeUpdate();
            log.debug("User with email " + smtp.getEmail() + " was deleted");
        } catch (SQLException ex) {
            log.debug("Exception: " + ex);
            throw ex;
        }
        return record;
    }

    /**
     * Updates an smtp setting Updated this method so that the user must update
     * all info at once
     * @param smtp  The smtp to update
     * @return The number of record updated (should always be 1)
     * @throws SQLException
     */
    public int updateSmtp(Smtp smtp) throws SQLException {
        String query = "UPDATE SMTP_SETTINGS SET UserName = ?, UserEmailPassword = ?, "
                + "SmtpUrl = ?, SmtpPortNumber = ?, Reminder = ?, defaultSmtp = ?  WHERE userEmail  = ?";
        int record;

        try (Connection connection = DriverManager.getConnection(url, username, password);
                PreparedStatement updateSmtp = connection.prepareStatement(query);) {

            if (smtp.getDefaultSmtp() == 1) {
                setOldDefaultSmtpToFalse();
            }

            updateSmtp.setString(1, smtp.getUsername());
            updateSmtp.setString(2, smtp.getPassword());
            updateSmtp.setString(3, smtp.getUrl());
            updateSmtp.setInt(4, smtp.getPortNumber());
            updateSmtp.setInt(5, smtp.getReminder());
            updateSmtp.setInt(6, smtp.getDefaultSmtp());
            updateSmtp.setString(7, smtp.getEmail());

            record = updateSmtp.executeUpdate();
            log.debug("Updated record for user: " + smtp.getEmail());
        } catch (SQLException ex) {
            log.debug("Exception: " + ex);
            throw ex;
        }
        return record;
    }

    /**
     * Finds the default smtp setting
     * @return A string value of the smtp setting
     * @throws SQLException
     */
    public Smtp findDefaultSmtp() throws SQLException {
        String query = "SELECT * FROM SMTP_SETTINGS WHERE DefaultSmtp = 1";
        Smtp s = null;

        try (Connection connection = DriverManager.getConnection(url, username, password);
                Statement findDefaultSmtp = connection.createStatement()) {

            try (ResultSet rs = findDefaultSmtp.executeQuery(query)) {
                s = convertResultSet(rs);
                log.debug("Found default SMTP setting: " + s.getEmail());
            }
        } catch (SQLException ex) {
            log.debug("Exception: " + ex);
            throw ex;
        }
        return s;
    }

    /**
     * Finds the default smtp setting
     * @param email     The email to search from
     * @return A string value of the smtp setting
     * @throws SQLException
     */
    public Smtp findSmtp(String email) throws SQLException {
        String query = "SELECT * FROM SMTP_SETTINGS WHERE UserEmail = ?";
        Smtp s = new Smtp();

        try (Connection connection = DriverManager.getConnection(url, username, password);
                PreparedStatement findSmtp = connection.prepareStatement(query);) {

            findSmtp.setString(1, email);

            ResultSet rs = findSmtp.executeQuery();
            s = convertResultSet(rs);
            log.debug("Found default SMTP setting: " + email);
        } catch (SQLException ex) {
            log.debug("Exception: " + ex);
            throw ex;
        }

        return s;
    }

    /**
     * Sets the old default Smtp to false
     * @throws SQLException
     * @return The number of record changed
     */
    public int setOldDefaultSmtpToFalse() throws SQLException {
        String query = "UPDATE SMTP_SETTINGS SET DefaultSmtp = 0 WHERE DefaultSmtp = ?";
        int defaultSmtp = 1;
        int record;

        try (Connection connection = DriverManager.getConnection(url, username, password);
                PreparedStatement setDefaultSmtp = connection.prepareStatement(query);) {

            setDefaultSmtp.setInt(1, defaultSmtp);

            record = setDefaultSmtp.executeUpdate();
            log.debug("Default Smtp updated");
        } catch (SQLException ex) {
            log.debug("Exception: " + ex);
            throw ex;
        }
        return record;
    }

    /**
     * Returns an stmp from a resultset
     * @param rs The resultset passed
     * @return The smtp object
     * @throws SQLException
     */
    private Smtp convertResultSet(ResultSet rs) throws SQLException {
        Smtp smtp = new Smtp();

        try {
            while (rs.next()) {
                smtp.setUsername(rs.getString("UserName"));
                smtp.setEmail(rs.getString("UserEmail"));
                smtp.setPassword(rs.getString("UserEmailPassword"));
                smtp.setUrl(rs.getString("SmtpUrl"));
                smtp.setPortNumber(rs.getInt("SmtpPortNumber"));
                smtp.setReminder(rs.getInt("Reminder"));
                smtp.setDefaultSmtp(rs.getInt("DefaultSmtp"));
            }
        } catch (SQLException ex) {
            log.debug("Exception: " + ex);
            throw ex;
        }
        return smtp;
    }
} // End class
