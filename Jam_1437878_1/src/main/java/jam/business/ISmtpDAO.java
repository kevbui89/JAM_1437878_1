package jam.business;

import jam.entities.Smtp;
import java.sql.SQLException;

/**
 * Interface for CRUD methods
 * 
 * @author Kevin
 * @version 1.0
 */

public interface ISmtpDAO {
    
     // Create
    public int createSmtp(Smtp s) throws SQLException;
    
    // Delete
    public int deleteSmtp(Smtp smtp) throws SQLException;
    
    // Update
    public int updateSmtp(Smtp s) throws SQLException;
    
    // Read
    public Smtp findDefaultSmtp() throws SQLException;
    
    public Smtp findSmtp(String email) throws SQLException;
 
}
