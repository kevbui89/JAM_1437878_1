package jam.business;

import jam.entities.AppRecord;
import jam.entities.Configuration;
import jam.entities.Smtp;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements the IAppRecDAO interface methods that are used to fetch and
 * manipulate data from the database
 * @author Kevin
 */
public class AppRecImpl implements IAppRecDAO {

    private Configuration config;
    private String url;
    private String username;
    private String password;
    private final Logger log = LoggerFactory.getLogger(this.getClass().getName());

    /**
     * Default constructor
     */
    public AppRecImpl() {
        super();
    }

    /**
     * One parameter constructor
     * @param config    The configuration of the database
     */
    public AppRecImpl(Configuration config) {
        this.config = config;
        this.username = config.getUserName();
        this.password = config.getPassword();
        this.url = config.getUrl();
    }

    /**
     * Creates an appointment record
     * @param ar    The appointment record
     * @throws SQLException
     */
    public int createAppRec(AppRecord ar) throws SQLException {
        String query = "INSERT INTO APPOINTMENTRECORD (Title, Loc, StartTime, EndTime, "
                + "Details, WholeDay, AppointmentGroup, AlarmReminderReq) VALUES (?,?,?,?,?,?,?,?)";
        int record;

        try (Connection connection = DriverManager.getConnection(url, username, password);
                PreparedStatement createAppRec = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);) {

            createAppRec.setString(1, ar.getTitle());
            createAppRec.setString(2, ar.getLocation());
            createAppRec.setTimestamp(3, ar.getStartTime());
            createAppRec.setTimestamp(4, ar.getEndTime());
            createAppRec.setString(5, ar.getDetails());
            createAppRec.setInt(6, ar.getWholeDay());
            createAppRec.setInt(7, ar.getAppGroup());
            createAppRec.setInt(8, ar.getAlarm());

            record = createAppRec.executeUpdate();
            String cTitle = "";
            try (ResultSet rs = createAppRec.getGeneratedKeys()) {
                if (rs.next()) {
                    cTitle = rs.getString(1);
                }
                log.debug("An appointment record was created: " + cTitle);
            }
        } catch (SQLException ex) {
            log.debug("Exception: " + ex);
            throw ex;
        }

        return record;
    }

    /**
     * Deletes an appointment record
     *
     * @param ar The appointment record
     * @return The number of record deleted (should always be 1)
     * @throws SQLException
     */
    public int deleteAppRec(AppRecord ar) throws SQLException {
        String query = "DELETE FROM APPOINTMENTRECORD WHERE AppointmentID = ?";
        int record;

        try (Connection connection = DriverManager.getConnection(url, username, password);
                PreparedStatement deleteAppRec = connection.prepareStatement(query);) {

            deleteAppRec.setInt(1, ar.getAppId());

            record = deleteAppRec.executeUpdate();
            log.debug("Appointment with ID " + ar.getAppId() + " was deleted");
        } catch (SQLException ex) {
            log.debug("Exception: " + ex);
            throw ex;
        }
        return record;
    }

    /**
     * Updates an appointment Updated this method so that the user must update
     * all info at once
     * @param ar    The appointment record
     * @return The number of record updated (should always be 1)
     * @throws SQLException
     */
    public int updateAppRec(AppRecord ar) throws SQLException {
        String query = "UPDATE APPOINTMENTRECORD SET Title = ?, Loc = ?, StartTime = ?, "
                + "EndTime = ?, Details = ?, WholeDay = ?, AlarmReminderReq = ? WHERE AppointmentID = ?";
        int record;

        try (Connection connection = DriverManager.getConnection(url, username, password);
                PreparedStatement updateAppRec = connection.prepareStatement(query);) {

            updateAppRec.setString(1, ar.getTitle());
            updateAppRec.setString(2, ar.getLocation());
            updateAppRec.setTimestamp(3, ar.getStartTime());
            updateAppRec.setTimestamp(4, ar.getEndTime());
            updateAppRec.setString(5, ar.getDetails());
            updateAppRec.setInt(6, ar.getWholeDay());
            updateAppRec.setInt(7, ar.getAlarm());
            updateAppRec.setInt(8, ar.getAppId());

            record = updateAppRec.executeUpdate();
            log.debug("Appointment record with id " + ar.getAppId() + " was updated");
        } catch (SQLException ex) {
            log.debug("Exception: " + ex);
            throw ex;
        }

        return record;
    }

    /**
     * Finds an appointment by ID
     *
     * @param ar The appointment record
     * @return A string value of the appointment
     * @throws SQLException
     */
    public AppRecord findAppById(AppRecord ar) throws SQLException {
        String query = "SELECT * FROM APPOINTMENTRECORD WHERE AppointmentID = ?";
        AppRecord appointment = null;

        try (Connection connection = DriverManager.getConnection(url, username, password);
                PreparedStatement findAppById = connection.prepareStatement(query);) {

            findAppById.setInt(1, ar.getAppId());

            try (ResultSet rs = findAppById.executeQuery()) {
                while (rs.next()) {
                    int id = rs.getInt("AppointmentID");
                    String title = rs.getString("Title");
                    String loc = rs.getString("Loc");
                    Timestamp start = rs.getTimestamp("StartTime");
                    Timestamp end = rs.getTimestamp("EndTime");
                    String details = rs.getString("Details");
                    int wholeDay = rs.getInt("WholeDay");
                    int appGroup = rs.getInt("AppointmentGroup");
                    int alarm = rs.getInt("AlarmReminderReq");

                    appointment = new AppRecord(id, title, loc, start, end, details,
                            wholeDay, appGroup, alarm);
                }
            }
            log.debug("Found appointment with id: " + ar.getAppId());
        } catch (SQLException ex) {
            log.debug("Exception: " + ex);
            throw ex;
        }
        return appointment;
    }

    /**
     * Returns a list of appointment IDs
     * @param ar The appointment record
     * @return List of IDs
     * @throws SQLException
     */
    public List<AppRecord> getAppFromTitle(AppRecord ar) throws SQLException {
        List<AppRecord> appointments = new ArrayList<AppRecord>();
        String query = "SELECT * FROM APPOINTMENTRECORD WHERE TITLE = ?";

        try (Connection connection = DriverManager.getConnection(url, username, password);
                PreparedStatement getId = connection.prepareStatement(query);) {

            getId.setString(1, ar.getTitle());

            try (ResultSet rs = getId.executeQuery()) {
                appointments = getListAppointmentRecord(rs);
            }
        } catch (SQLException ex) {
            log.debug("Exception: " + ex);
            throw ex;
        }
        log.debug("An ID was added to the list");
        return appointments;
    }

    /**
     * Returns a list of all appointments between two dates
     * @param start The start limit
     * @param end The end limit
     * @throws SQLException
     */
    public List<AppRecord> findAppBetweenTwoDates(Timestamp start, Timestamp end) throws SQLException {
        List<AppRecord> appointments = new ArrayList<AppRecord>();
        String query = "SELECT * FROM APPOINTMENTRECORD WHERE StartTime BETWEEN ? AND ?";

        try (Connection connection = DriverManager.getConnection(url, username, password);
                PreparedStatement getId = connection.prepareStatement(query);) {

            getId.setTimestamp(1, start);
            getId.setTimestamp(2, end);

            try (ResultSet rs = getId.executeQuery()) {
                appointments = getListAppointmentRecord(rs);
            }
        } catch (SQLException ex) {
            log.debug("Exception: " + ex);
            throw ex;
        }
        log.debug("An ID was added to the list");
        return appointments;
    }

    /**
     * Returns all the appointments for a specific month
     * @param month The month the user is looking up
     * @return
     * @throws SQLException
     */
    public List<AppRecord> findAppForMonth(String month) throws SQLException {
        String query = "SELECT * FROM APPOINTMENTRECORD WHERE MONTH(StartTime) = ? ORDER BY StartTime";
        List<AppRecord> appointments = new ArrayList<AppRecord>();
        int m = getIntMonth(month);

        try (Connection connection = DriverManager.getConnection(url, username, password);
                PreparedStatement findAppForMonth = connection.prepareStatement(query);) {

            findAppForMonth.setInt(1, m);

            try (ResultSet rs = findAppForMonth.executeQuery()) {
                appointments = getListAppointmentRecord(rs);
            }
        } catch (SQLException ex) {
            log.debug("Exception: " + ex);
            throw ex;
        }
        log.debug("An ID was added to the list of appointments for the month of " + month);

        return appointments;
    }

    /**
     * Finds all the appointments for a specific week
     * @param day
     * @throws SQLException
     */
    public List<AppRecord> findAppForWeek(String day) throws SQLException {
        String query = "SELECT * FROM APPOINTMENTRECORD WHERE StartTime BETWEEN ? AND ? ORDER BY StartTime ASC";
        List<AppRecord> appointments = new ArrayList<AppRecord>();
        List<Timestamp> week = getWeek(day);

        try (Connection connection = DriverManager.getConnection(url, username, password);
                PreparedStatement findAppForWeek = connection.prepareStatement(query);) {

            findAppForWeek.setTimestamp(1, week.get(0));
            findAppForWeek.setTimestamp(2, week.get(1));

            try (ResultSet rs = findAppForWeek.executeQuery()) {
                appointments = getListAppointmentRecord(rs);
            }
        } catch (SQLException ex) {
            log.debug("Exception: " + ex);
            throw ex;
        }
        log.debug("An ID was added to the list of appointments for the week of " + day);

        return appointments;
    }

    /**
     * Returns all the appointments of a specific day
     * @param day The day from which the user is looking up appointments
     * @return
     * @throws SQLException
     */
    public List<AppRecord> findAppForDay(String day) throws SQLException {
        List<AppRecord> appointments = new ArrayList<AppRecord>();
        String query = "SELECT * FROM APPOINTMENTRECORD WHERE StartTime BETWEEN ? AND ?";
        List<Timestamp> list = getDay(day);

        try (Connection connection = DriverManager.getConnection(url, username, password);
                PreparedStatement findAppForDay = connection.prepareStatement(query);) {

            findAppForDay.setTimestamp(1, list.get(0));
            findAppForDay.setTimestamp(2, list.get(1));

            try (ResultSet rs = findAppForDay.executeQuery()) {
                appointments = getListAppointmentRecord(rs);
            }
        } catch (SQLException ex) {
            log.debug("Exception: " + ex);
            throw ex;
        }
        log.debug("An ID was added to the list of appointments for the day of " + day);

        return appointments;
    }

    /**
     * Find an app at a specific date and time
     * @param appointment The time of the appointment
     * @return The appointment
     * @throws SQLException
     */
    public AppRecord findApp(Timestamp appointment) throws SQLException {
        String query = "SELECT * FROM APPOINTMENTRECORD WHERE StartTime = ?";
        AppRecord app = null;

        try (Connection connection = DriverManager.getConnection(url, username, password);
                PreparedStatement findApp = connection.prepareStatement(query);) {

            findApp.setTimestamp(1, appointment);

            try (ResultSet rs = findApp.executeQuery()) {
                while (rs.next()) {
                    int id = rs.getInt("AppointmentID");
                    String title = rs.getString("Title");
                    String loc = rs.getString("Loc");
                    Timestamp start = rs.getTimestamp("StartTime");
                    Timestamp end = rs.getTimestamp("EndTime");
                    String details = rs.getString("Details");
                    int wholeDay = rs.getInt("WholeDay");
                    int appGroup = rs.getInt("AppointmentGroup");
                    int alarm = rs.getInt("AlarmReminderReq");

                    app = new AppRecord(id, title, loc, start, end, details,
                            wholeDay, appGroup, alarm);
                }
            }
        } catch (SQLException ex) {
            log.debug("Exception: " + ex);
            throw ex;
        }
        
        return app;
    }

    /**
     * Find an app at a specific date and time Made for testing purposes for the
     * email
     * @param appointment The time of the appointment
     * @param appointment2  The time of the appointment plus 3 minutes
     * @return The appointment
     * @throws SQLException
     */
    public List<AppRecord> findAppListForSpecificTimePlus3(Timestamp appointment, Timestamp appointment2) throws SQLException {
        String query = "SELECT * FROM APPOINTMENTRECORD WHERE StartTime BETWEEN ? AND ?";
        List<AppRecord> appointments = new ArrayList<AppRecord>();

        try (Connection connection = DriverManager.getConnection(url, username, password);
                PreparedStatement findApp = connection.prepareStatement(query);) {

            findApp.setTimestamp(1, appointment);
            findApp.setTimestamp(2, appointment2);

            try (ResultSet rs = findApp.executeQuery()) {
                appointments = getListAppointmentRecord(rs);
            }
        } catch (SQLException ex) {
            log.debug("Exception: " + ex);
            throw ex;
        }

        return appointments;
    }

    /**
     * Returns a list of appointment records form a resultset
     * @param rs The resultset passed
     * @return The list of appointment record
     * @throws SQLException
     */
    private List<AppRecord> getListAppointmentRecord(ResultSet rs) throws SQLException {
        List<AppRecord> list = new ArrayList<AppRecord>();

        try {
            while (rs.next()) {
                int id = rs.getInt("AppointmentID");
                String title = rs.getString("Title");
                String loc = rs.getString("Loc");
                Timestamp s = rs.getTimestamp("StartTime");
                Timestamp e = rs.getTimestamp("EndTime");
                String details = rs.getString("Details");
                int wholeDay = rs.getInt("WholeDay");
                int appGroup = rs.getInt("AppointmentGroup");
                int alarm = rs.getInt("AlarmReminderReq");

                AppRecord app = new AppRecord(id, title, loc, s, e, details, wholeDay,
                        appGroup, alarm);

                list.add(app);
            }
        } catch (SQLException ex) {
            log.debug("Exception: " + ex);
            throw ex;
        }
        return list;
    }

    /**
     * Returns the string month into the appropriate Integer
     * @param month The month to be converted
     * @return The integer value of the month
     */
    public int getIntMonth(String month) {
        int m = 0;
        month = month.toLowerCase();

        switch (month) {
            case "january":
                m = 1;
                break;
            case "february":
                m = 2;
                break;
            case "march":
                m = 3;
                break;
            case "april":
                m = 4;
                break;
            case "may":
                m = 5;
                break;
            case "june":
                m = 6;
                break;
            case "july":
                m = 7;
                break;
            case "august":
                m = 8;
                break;
            case "september":
                m = 9;
                break;
            case "october":
                m = 10;
                break;
            case "november":
                m = 11;
                break;
            case "december":
                m = 12;
                break;
            default:
                throw new IllegalArgumentException("Not a valid month");
        }
        return m;
    }

    /**
     * Gets the week from sunday to saturday
     * @return A list of timestamps
     */
    private List<Timestamp> getWeek(String day) {
        String pattern = "^(19[5-9][0-9]|20[0-4][0-9]|2050)[-](0[1-9]|1[0-2])[-](0[1-9]|[12][0-9]|3[01])$";
        String beginning = "00:00:00";
        int weekOfYear = 0;
        Calendar c = GregorianCalendar.getInstance();
        java.util.Date sun, sat = null;
        java.sql.Timestamp sunday, saturday = null;
        List<Timestamp> list = new ArrayList<Timestamp>();

        if (!day.matches(pattern)) {
            throw new IllegalArgumentException("Invalid format");
        }

        Timestamp start = Timestamp.valueOf(day + " " + beginning);
        c.setTime(start);
        // Get the week number
        weekOfYear = c.get(Calendar.WEEK_OF_YEAR);
        c.set(Calendar.WEEK_OF_YEAR, weekOfYear);

        // Get the first day of the week
        c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        sun = c.getTime();
        sunday = new java.sql.Timestamp(sun.getTime());
        list.add(sunday);

        // Get the last day of the week
        c.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
        sat = c.getTime();
        saturday = new java.sql.Timestamp(sat.getTime());
        list.add(saturday);

        return list;
    }

    /**
     * Returns an arraylist of the start of the day and end of the day
     * @param day The day to get start and end from
     * @return A list of the start and end
     */
    private List<Timestamp> getDay(String day) {
        String pattern = "^(19[5-9][0-9]|20[0-4][0-9]|2050)[-](0[1-9]|1[0-2])[-](0[1-9]|[12][0-9]|3[01])$";
        String beginning = "00:00:00";
        String ending = "23:59:59";
        List<Timestamp> list = new ArrayList<Timestamp>();

        if (!day.matches(pattern)) {
            throw new IllegalArgumentException("Invalid format");
        }

        Timestamp start = Timestamp.valueOf(day + " " + beginning);
        list.add(start);
        log.debug("Start of day was added");
        Timestamp end = Timestamp.valueOf(day + " " + ending);
        list.add(end);
        log.debug("End of day was added");

        return list;
    }
    
    /**
     * Returns the single apppointment selected by the user
     *
     * @param list The list of all appointments with the same title
     * @param position The position wanted
     * @return
     * @throws SQLException
     */
    public AppRecord getAppIdSingle(List<AppRecord> list, int position) throws SQLException {
        return list.get(position);
    }

    /**
     * Check if it is time for the reminder
     * @return Boolean value if the it is time for the reminder or not
     * @throws SQLException
     */
    public boolean isTimeForReminder() throws SQLException {
        List<AppRecord> list = new ArrayList<AppRecord>();
        boolean isAlarm = false;
        SmtpImpl s = new SmtpImpl(config);
        Smtp defaultSmtp = s.findDefaultSmtp();

        LocalDateTime alarmTime = LocalDateTime.now().plusMinutes(defaultSmtp.getReminder());
        LocalDateTime alarmTime2 = LocalDateTime.now().plusMinutes(defaultSmtp.getReminder() + 3);
        Timestamp t = Timestamp.valueOf(alarmTime);
        Timestamp t2 = Timestamp.valueOf(alarmTime2);
        try {
            list = findAppListForSpecificTimePlus3(t, t2);
        } catch (SQLException ex) {
            log.debug("Exception: " + ex);
            throw ex;
        }

        if (list.size() > 0) {
            isAlarm = true;
        }

        return isAlarm;
    }
}
