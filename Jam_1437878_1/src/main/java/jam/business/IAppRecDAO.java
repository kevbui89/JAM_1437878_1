package jam.business;

import jam.entities.AppRecord;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

/**
 * The interface for the appointment records
 * @author Kevin
 */
public interface IAppRecDAO {
    
    // Create
    public int createAppRec(AppRecord ar) throws SQLException;
    
    // Delete    
    public int deleteAppRec(AppRecord ar) throws SQLException;
    
    // Update
    public int updateAppRec(AppRecord ar) throws SQLException;
    
    // Read
    public AppRecord findAppById(AppRecord ar) throws SQLException;
    
    public List<AppRecord> getAppFromTitle(AppRecord ar) throws SQLException;
    
    public List<AppRecord> findAppBetweenTwoDates(Timestamp start, Timestamp end) throws SQLException;

    public List<AppRecord> findAppForMonth(String month) throws SQLException; 
    
    public List<AppRecord> findAppForWeek(String day) throws SQLException;

    public List<AppRecord> findAppForDay(String day) throws SQLException;
    
    public AppRecord findApp(Timestamp appointment) throws SQLException; 
}
