package jam.business;

import jam.entities.AppGroupRecord;
import jam.entities.Configuration;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The implementation of the IAppGroupRecDAO interface methods that are used to manipulate
 * the data from the database
 *
 * @author Kevin
 */
public class AppGroupRecImpl implements IAppGroupRecDAO {

    private Configuration config;
    private String url;
    private String username;
    private String password;
    private final Logger log = LoggerFactory.getLogger(this.getClass().getName());

    /**
     * Default constructor
     */
    public AppGroupRecImpl() {
        super();
    }

    /**
     * One parameter constructor
     * @param config    The configuration of the database
     */
    public AppGroupRecImpl(Configuration config) {
        this.config = config;
        this.username = config.getUserName();
        this.password = config.getPassword();
        this.url = config.getUrl();
        log.debug("Config=" + config.toString());
    }

    /**
     * Creates a group record
     *
     * @param agr The group record to create
     * @throws SQLException
     */
    public int createAppGroupRec(AppGroupRecord agr) throws SQLException {
        String query = "INSERT INTO APPOINTMENTGROUPRECORD (GroupName, Color) VALUES (?,?);";
        int record;

        try (Connection connection = DriverManager.getConnection(url, username, password);
                PreparedStatement createAppGroupRec = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);) {
            String upperCase = agr.getColor().toUpperCase();
            createAppGroupRec.setString(1, agr.getGroupName());
            createAppGroupRec.setString(2, upperCase);

            record = createAppGroupRec.executeUpdate();
            try (ResultSet rs = createAppGroupRec.getGeneratedKeys()) {
                String gName = "";
                if (rs.next()) {
                    gName = rs.getString(1);
                }
                log.debug("New record User Name is " + gName);
            } catch (SQLException ex) {
                log.debug("Exception: " + ex);
                throw ex;
            }
        }

        return record;
    }

    /**
     * Deletes a group record
     * @param agr The group record to delete
     * @throws SQLException
     */
    public int deleteAppGroupRec(AppGroupRecord agr) throws SQLException {
        String query = "DELETE FROM APPOINTMENTGROUPRECORD WHERE GroupNumber = ?";
        int record;

        try (Connection connection = DriverManager.getConnection(url, username, password);
                PreparedStatement deleteAppGroupRec = connection.prepareStatement(query);) {

            deleteAppGroupRec.setInt(1, agr.getId());

            record = deleteAppGroupRec.executeUpdate();
            log.debug("Appointment group record number " + agr.getId() + " was deleted");
        } catch (SQLException ex) {
            log.debug("Exception: " + ex);
            throw ex;
        }

        return record;
    }

    /**
     * Updates an appointment group record
     * @param agr The group record to update
     * @return The number of record updated (should always be 1)
     * @throws SQLException
     */
    public int updateAppGroupRec(AppGroupRecord agr) throws SQLException {
        String query = "UPDATE APPOINTMENTGROUPRECORD SET GroupName = ?, Color = ? WHERE GroupNumber = ?";
        int record;

        try (Connection connection = DriverManager.getConnection(url, username, password);
                PreparedStatement updateAppGroupRec = connection.prepareStatement(query);) {
            agr.setColor(agr.getColor().toUpperCase());

            updateAppGroupRec.setString(1, agr.getGroupName());
            updateAppGroupRec.setString(2, agr.getColor());
            updateAppGroupRec.setInt(3, agr.getId());

            record = updateAppGroupRec.executeUpdate();
            log.debug("Group number " + agr.getId() + " was updated");
        } catch (SQLException ex) {
            log.debug("Exception: " + ex);
            throw ex;
        }
        return record;
    }

    /**
     * Finds the appointment group record by group name
     * @param agr The group record to look for
     * @return A string value of the appointment
     * @throws SQLException
     */
    public AppGroupRecord findAppGroupRec(AppGroupRecord agr) throws SQLException {
        String query = "SELECT * FROM APPOINTMENTGROUPRECORD WHERE GroupName = ?";

        try (Connection connection = DriverManager.getConnection(url, username, password);
                PreparedStatement findAppGroupRec = connection.prepareStatement(query);) {

            findAppGroupRec.setString(1, agr.getGroupName());

            try (ResultSet rs = findAppGroupRec.executeQuery()) {
                while (rs.next()) {
                    int id = rs.getInt("GroupNumber");
                    String gName = rs.getString("GroupName");
                    String color = rs.getString("Color");

                    agr = new AppGroupRecord(id, gName, color);
                }
                log.debug("Found appointment group: " + agr.getGroupName());
            }
        } catch (SQLException ex) {
            log.debug("Exception: " + ex);
            throw ex;
        }
        return agr;
    }
    
    /**
     * Find the group by group number
     * @return  A list of appointment group records
     * @throws SQLException 
     */
    public List<AppGroupRecord> findAppGroupRec() throws SQLException {
        String query = "SELECT * FROM APPOINTMENTGROUPRECORD";
        List<AppGroupRecord> list = new ArrayList<AppGroupRecord>();

        try (Connection connection = DriverManager.getConnection(url, username, password);
                PreparedStatement findAppGroupRec = connection.prepareStatement(query);) {

            try (ResultSet rs = findAppGroupRec.executeQuery()) {
                while (rs.next()) {
                    int id = rs.getInt("GroupNumber");
                    String gName = rs.getString("GroupName");
                    String color = rs.getString("Color");

                    AppGroupRecord agr = new AppGroupRecord(id, gName, color);
                    log.debug("Found appointment group: " + agr.getGroupName());
                    
                    list.add(agr);
                }
            }
        } catch (SQLException ex) {
            log.debug("Exception: " + ex);
            throw ex;
        }
        return list;
    }
}
