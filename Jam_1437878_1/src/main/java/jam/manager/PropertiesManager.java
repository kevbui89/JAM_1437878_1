package jam.manager;

import jam.entities.Configuration;
import static java.nio.file.Files.newInputStream;
import static java.nio.file.Files.newOutputStream;
import static java.nio.file.Paths.get;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

/**
 * This class writes and loads properties
 * @author Kevin
 */
public class PropertiesManager {
    
    /**
     * Returns a Configuration object with the contents of the properties file
     * @return  dbConfig    The bean loaded with the properties
     * @throws IOException 
     */
    public Configuration loadTextProperties(String path, String fileName, Configuration dbConfig) throws IOException {
        Properties properties = new Properties();
        Path txtFile = get(path, fileName + ".properties");
        
        if (Files.exists(txtFile)) {
            try (InputStream propFileStream = newInputStream(txtFile);) {
                properties.load(propFileStream);
            }
            dbConfig.setUserName(properties.getProperty("username"));
            dbConfig.setPassword(properties.getProperty("password"));
            dbConfig.setUrl(properties.getProperty("url"));
        }
        return dbConfig;
    }
    
    /**
     * Updates a Configuration object with the contents of the properties file
     * @param path      The path of the properties file
     * @param fileName  The name of the properties file
     * @param dbConfig  The configuration object
     * @return          A boolean if found or not
     * @throws java.io.IOException
     */
    public boolean loadTextPropertiesBoolean(String path, String fileName, Configuration dbConfig) throws IOException {

        boolean found = false;
        Properties properties = new Properties();

        Path txtFile = get(path, fileName + ".properties");

        // File must exist
        if (Files.exists(txtFile)) {
            try (InputStream propFileStream = newInputStream(txtFile);) {
                properties.load(propFileStream);
            }
            dbConfig.setUserName(properties.getProperty("userName"));
            dbConfig.setPassword(properties.getProperty("password"));
            dbConfig.setUrl(properties.getProperty("userEmailAddress"));

            found = true;
        }
        return found;
    }

    
    /**
     * Write the new properties to the file
     * @param path      The path of the properties file
     * @param fileName  The name of the properties file
     * @param dbConfig  The configuration object
     * @throws IOException 
     */
    public void writeTextProperties(String path, String fileName, Configuration dbConfig) throws IOException {
        Properties properties = new Properties();
        
        properties.setProperty("username", dbConfig.getUserName());
        properties.setProperty("password", dbConfig.getPassword());
        properties.setProperty("url", dbConfig.getUrl());
        
        Path txtFile = get(path, fileName + ".properties");
        
        try (OutputStream propFileStream = newOutputStream(txtFile)) {
            properties.store(propFileStream, "Database Configuration Credentials");
        }
    }
}
