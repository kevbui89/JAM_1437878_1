package jam.app.controller;

import jam.business.AppGroupRecImpl;
import jam.business.AppRecImpl;
import jam.business.IAppGroupRecDAO;
import jam.business.IAppRecDAO;
import jam.entities.AppGroupRecord;
import jam.entities.AppRecord;
import jam.entities.DailyAppointmentRowBean;
import jam.entities.Configuration;
import jam.manager.PropertiesManager;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Daily View Controller
 *
 * @author Kevin
 */
public class DailyViewFXMLController {

    private final Logger log = LoggerFactory.getLogger(this.getClass()
            .getName());
    private int dayCounter = 0;
    private int day;
    private LocalDateTime ldt;
    private IAppRecDAO arDAO;
    private IAppGroupRecDAO agrDAO;
    private Configuration config;
    private List<AppRecord> list;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button previousBtn;

    @FXML
    private Button nextDayBtn;

    @FXML
    private Label dailyTitle;

    @FXML
    private TableView<DailyAppointmentRowBean> dailyView;

    @FXML
    private TableColumn<DailyAppointmentRowBean, String> dailyDateTimeColumn;

    @FXML
    private TableColumn<DailyAppointmentRowBean, String> dailyAppInfoColumn;

    /**
     * Default constructor
     */
    public DailyViewFXMLController() throws IOException {
        super();
        setUp();
        this.arDAO = new AppRecImpl(config);
        this.agrDAO = new AppGroupRecImpl(config);
        log.info("Default Constructor");
    }

    /**
     * Sets up the Configuration
     */
    public void setUp() throws IOException {
        PropertiesManager pm = new PropertiesManager();
        config = new Configuration();
        config = pm.loadTextProperties("src/main/resources", "JamDatabaseProperties", config);
    }

    public void setTitle(String text) {
        this.dailyTitle.setText(text);
    }

    public String getDate() {
        return getDateStr();
    }

    public @FXML
    void showNextDay(ActionEvent event) {
        log.debug("Entered next day");
        dayCounter--;
        this.dailyTitle.setText(getDate());
        dailyView.setItems(fillDailyView());
        dailyView.refresh();
    }

    @FXML
    void showPreviousDay(ActionEvent event) {
        log.debug("Entered next day");
        dayCounter++;
        this.dailyTitle.setText(getDate());
        dailyView.setItems(fillDailyView());
        dailyView.refresh();
    }

    @FXML
    void initialize() throws SQLException {
        dailyDateTimeColumn.setCellValueFactory(cellData -> cellData.getValue()
                .dateTimeProperty());
        dailyAppInfoColumn.setCellValueFactory(cellData -> cellData.getValue()
                .titleProperty());

        dailyView.setOnMouseClicked((MouseEvent event) -> {
            if (event.getButton().equals(MouseButton.PRIMARY)) {
                dialogSingleCellDetails(dailyView.getSelectionModel().getSelectedItem().toString());
                log.debug("Daily View: " + dailyView.getSelectionModel().getSelectedItem());
            }
        });

        ldt = LocalDateTime.now().minusDays(dayCounter);
        day = ldt.getDayOfMonth();
        displayTheTable();
    }

    /**
     * Displays the monthly view
     *
     * @throws SQLException
     */
    public void displayTheTable() {
        dailyView.setItems(fillDailyView());
    }

    /**
     * Displays the Daily view
     *
     * @return ObservableList of AppointmentBean
     */
    private ObservableList<DailyAppointmentRowBean> fillDailyView() {
        ldt = LocalDateTime.now().minusDays(dayCounter);
        AppGroupRecord agr = null;
        List<AppGroupRecord> agrList = new ArrayList<AppGroupRecord>();
        findApps();
        ObservableList<DailyAppointmentRowBean> rowBean = FXCollections.observableArrayList();
        LocalDateTime ldt = LocalDate.now().atStartOfDay().minusDays(dayCounter);
        Timestamp start = Timestamp.valueOf(ldt);
        LocalDateTime loop = start.toLocalDateTime();

        for (int i = 0; i < 48; i++) {
            Timestamp add = Timestamp.valueOf(loop);
            DailyAppointmentRowBean wrb = new DailyAppointmentRowBean();
            wrb.setDateTime(add.toString().substring(11, 19));
            if (!list.isEmpty()) {
                for (AppRecord ar : list) {
                    log.debug("Inside for loop");
                    if (ar.getStartTime().equals(add)) {
                        wrb.setTitle(ar.getAppId() + "\n" + ar.getTitle() + "\n"
                                + ar.getStartTime());
                    }
                    if (add.after(ar.getStartTime()) && add.before(ar.getEndTime())) {
                        try {
                            agrList = agrDAO.findAppGroupRec();
                            for (AppGroupRecord appGroupRec : agrList) {
                                if (appGroupRec.getId() == ar.getAppGroup()) {
                                    String color = appGroupRec.getColor();
                                    renderCell(dailyAppInfoColumn, color);
                                }
                            }
                        } catch (SQLException e) {
                            log.debug(e.getMessage());
                        }
                        wrb.setTitle(ar.getAppId() + "\n" + ar.getTitle() + "\n"
                                + ar.getStartTime());
                    }
                } // End list for loop
            } // End if list is not empty
            log.debug("Time " + add.toString());
            rowBean.add(wrb);
            loop = loop.plusMinutes(30);
        }
        return rowBean;
    }

    /**
     * Responsible for the contents of a cell to update the color of a cell
     *
     * @param tc
     */
    private void renderCell(TableColumn tc, String color) {
        tc.setCellFactory(column -> {
            return new TableCell<DailyAppointmentRowBean, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(item);
                    log.info("ITEM: " + item);
                    if (item == null || empty || item.equals("")) {
                        setText(null);
                        setStyle("");
                    } else {
                        setStyle("-fx-background-color: " + color + ";" + "-fx-text-fill: white;");
                        if (color.equals("#FFA500") || color.equals("#ADD8E6")) {
                            setStyle("-fx-background-color: " + color + ";" + "-fx-text-fill: white;");
                        }
                    }
                }
            };
        });
    }

    /**
     * Returns the string value of the date depending on the click event
     *
     * @return The date string
     */
    private String getDateStr() {
        String d = "";
        LocalDateTime now = LocalDateTime.now().minusDays(dayCounter);
        d = now.toString().substring(0, 10);
        return d;
    }

    /**
     * Display the string contents of a single cell.
     *
     * @param data
     */
    private void dialogSingleCellDetails(String data) {
        log.debug("In AppRec Form");
        log.debug("DATA: " + data.toString());

        try {
            if (data.substring(data.indexOf("title=") + 6).length() > 1) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AppRecordFormFXML.fxml"));
                loader.setResources(ResourceBundle.getBundle("MessageBundle"));
                Parent root = (DialogPane) loader.load();
                AppRecordFormFXMLController controller = loader.getController();
                controller.setAppRecDAOData(data.substring(data.indexOf("20"), data.indexOf("}")));
                Stage stage = new Stage();
                stage.setScene(new Scene(root));
                stage.show();
            } else {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AppRecordFormFXML.fxml"));
                loader.setResources(ResourceBundle.getBundle("MessageBundle"));
                Parent root = (DialogPane) loader.load();
                AppRecordFormFXMLController controller = loader.getController();
                Stage stage = new Stage();
                stage.setScene(new Scene(root));
                stage.show();
            }
        } catch (IOException e) {
            log.debug(e.getMessage());
        }
    }

    /**
     * Finds all appointments for the month
     */
    public void findApps() {
        log.info("Date: " + ldt.toString().substring(0, 10));
        try {
            list = arDAO.findAppForDay(ldt.toString().substring(0, 10));
            for (AppRecord ar : list) {
                log.info(ar.toString());
            }
        } catch (SQLException ex) {
            log.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    /**
     * Error pop up message
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(ResourceBundle.getBundle("MessageBundle").getString("ERRORDAO"));
        dialog.setHeaderText(ResourceBundle.getBundle("MessageBundle").getString("ERRORDAOTEXT"));
        dialog.setContentText(msg);
        dialog.show();
    }
}
