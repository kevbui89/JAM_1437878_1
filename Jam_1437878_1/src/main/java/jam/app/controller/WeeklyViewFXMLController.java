package jam.app.controller;

import jam.business.AppGroupRecImpl;
import jam.business.AppRecImpl;
import jam.business.IAppGroupRecDAO;
import jam.business.IAppRecDAO;
import jam.entities.AppGroupRecord;
import jam.entities.AppRecord;
import jam.entities.Configuration;
import jam.entities.DailyAppointmentRowBean;
import jam.entities.WeeklyAppointmentRowBean;
import jam.manager.PropertiesManager;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * Weekly View
 *
 * @author Kevin
 */
public class WeeklyViewFXMLController {

    private final Logger log = LoggerFactory.getLogger(this.getClass()
            .getName());
    private Calendar c;
    private int weekOfYear;
    private ObservableList<TablePosition> theCells;
    private List<AppRecord> list;
    private LocalDateTime ldt;
    private IAppRecDAO arDAO;
    private IAppGroupRecDAO agrDAO;
    private Configuration config;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button previousWeekBtn;

    @FXML
    private Button nextWeekBtn;

    @FXML
    private Label weeklyTitle;

    @FXML
    private BorderPane weeklyBorderPane;

    @FXML
    private TableView<WeeklyAppointmentRowBean> weeklyView;

    @FXML
    private TableColumn<WeeklyAppointmentRowBean, String> dateTimeColumn;

    @FXML
    private TableColumn<WeeklyAppointmentRowBean, String> sundayColumn;

    @FXML
    private TableColumn<WeeklyAppointmentRowBean, String> mondayColumn;

    @FXML
    private TableColumn<WeeklyAppointmentRowBean, String> tuesdayColumn;

    @FXML
    private TableColumn<WeeklyAppointmentRowBean, String> wednesdayColumn;

    @FXML
    private TableColumn<WeeklyAppointmentRowBean, String> thursdayColumn;

    @FXML
    private TableColumn<WeeklyAppointmentRowBean, String> fridayColumn;

    @FXML
    private TableColumn<WeeklyAppointmentRowBean, String> saturdayColumn;

    public WeeklyViewFXMLController() throws IOException {
        super();
        setUp();
        this.arDAO = new AppRecImpl(config);
        this.agrDAO = new AppGroupRecImpl(config);
        log.debug("Default Constructor");
        log.debug("Week #:" + weekOfYear);
    }

    /**
     * Sets up the Configuration
     */
    public void setUp() throws IOException {
        PropertiesManager pm = new PropertiesManager();
        config = new Configuration();
        config = pm.loadTextProperties("src/main/resources", "JamDatabaseProperties", config);
    }

    /**
     * Sets the title of the view
     *
     * @param text
     */
    public void setTitle(String text) {
        this.weeklyTitle.setText(text);
    }

    @FXML
    void showNextWeek(ActionEvent event) throws SQLException {
        log.debug("Week #:" + weekOfYear);
        weekOfYear++;
        if (weekOfYear > 52) {
            weekOfYear = 0;
        }
        if (weekOfYear < 0) {
            weekOfYear = 52;
        }
        weeklyView.setItems(fillWeekView());
        weeklyView.refresh();
    }

    @FXML
    void showPreviousWeek(ActionEvent event) throws SQLException {
        log.debug("Week #:" + weekOfYear);
        weekOfYear--;
        if (weekOfYear > 52) {
            weekOfYear = 0;
        }
        if (weekOfYear < 0) {
            weekOfYear = 52;
        }
        weeklyView.setItems(fillWeekView());
        weeklyView.refresh();
    }

    @FXML
    void initialize() throws SQLException {
        dateTimeColumn.setCellValueFactory(cellData -> cellData.getValue()
                .dateTimeProperty());
        sundayColumn.setCellValueFactory(cellData -> cellData.getValue()
                .sundayProperty());
        mondayColumn.setCellValueFactory(cellData -> cellData.getValue()
                .mondayProperty());
        tuesdayColumn.setCellValueFactory(cellData -> cellData.getValue()
                .tuesdayProperty());
        wednesdayColumn.setCellValueFactory(cellData -> cellData.getValue()
                .wednesdayProperty());
        thursdayColumn.setCellValueFactory(cellData -> cellData.getValue()
                .thursdayProperty());
        fridayColumn.setCellValueFactory(cellData -> cellData.getValue()
                .fridayProperty());
        saturdayColumn.setCellValueFactory(cellData -> cellData.getValue()
                .saturdayProperty());

        // Set table columns unsortable
        dateTimeColumn.setSortable(false);
        sundayColumn.setSortable(false);
        mondayColumn.setSortable(false);
        tuesdayColumn.setSortable(false);
        wednesdayColumn.setSortable(false);
        thursdayColumn.setSortable(false);
        fridayColumn.setSortable(false);
        saturdayColumn.setSortable(false);

        // Set datetime columns resizable to false
        sundayColumn.setResizable(false);
        mondayColumn.setResizable(false);
        tuesdayColumn.setResizable(false);
        wednesdayColumn.setResizable(false);
        thursdayColumn.setResizable(false);
        fridayColumn.setResizable(false);

        weeklyView.getColumns().addListener(new ListChangeListener() {
            @Override
            public void onChanged(Change change) {
                change.next();
                if (change.wasReplaced()) {
                    weeklyView.getColumns().clear();
                    weeklyView.getColumns().addAll(dateTimeColumn, sundayColumn,
                            mondayColumn, tuesdayColumn, wednesdayColumn,
                            thursdayColumn, fridayColumn, saturdayColumn);
                }
            }
        });

        weeklyView.getSelectionModel().setCellSelectionEnabled(true);

        theCells = weeklyView.getSelectionModel().getSelectedCells();
        theCells.addListener(this::showSingleCellDetails);

        ldt = LocalDate.now().atStartOfDay();
        c = GregorianCalendar.getInstance();
        weekOfYear = c.get(Calendar.WEEK_OF_YEAR);
        displayTheTable();
    }

    /**
     * Displays the monthly view
     */
    public void displayTheTable() throws SQLException {
        log.debug("In display the table");
        weeklyView.setItems(fillWeekView());
    }

    /**
     * Fills the weekly view
     *
     * @return
     * @throws SQLException
     */
    private ObservableList<WeeklyAppointmentRowBean> fillWeekView() throws SQLException {
        ObservableList<WeeklyAppointmentRowBean> rowBean = FXCollections.observableArrayList();
        Timestamp start = Timestamp.valueOf(ldt);
        List<AppGroupRecord> agrList = new ArrayList<AppGroupRecord>();
        c.setTime(start);
        c.set(Calendar.WEEK_OF_YEAR, weekOfYear);
        // Get all days of the week
        c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        java.util.Date sun = c.getTime();
        c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        java.util.Date mon = c.getTime();
        c.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
        java.util.Date tues = c.getTime();
        c.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY);
        java.util.Date wed = c.getTime();
        c.set(Calendar.DAY_OF_WEEK, Calendar.THURSDAY);
        java.util.Date thurs = c.getTime();
        c.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
        java.util.Date fri = c.getTime();
        c.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
        java.util.Date sat = c.getTime();
        Timestamp sunday = new java.sql.Timestamp(sun.getTime());
        Timestamp monday = new java.sql.Timestamp(mon.getTime());
        Timestamp tuesday = new java.sql.Timestamp(tues.getTime());
        Timestamp wednesday = new java.sql.Timestamp(wed.getTime());
        Timestamp thursday = new java.sql.Timestamp(thurs.getTime());
        Timestamp friday = new java.sql.Timestamp(fri.getTime());
        Timestamp saturday = new java.sql.Timestamp(sat.getTime());
        LocalDateTime s = sunday.toLocalDateTime();
        LocalDateTime sa = saturday.toLocalDateTime();

        // List of dates of the week
        List<Timestamp> ldtList = new ArrayList<>(Arrays.asList(sunday, monday, tuesday, wednesday, thursday, friday, saturday));

        this.weeklyTitle.setText(s.toString().substring(0, 10) + " - " + sa.toString().substring(0, 10));

        // Only Sunday seems to populate for the weekly view
        for (int i = 0; i < 48; i++) {
            Timestamp add = Timestamp.valueOf(s);
            WeeklyAppointmentRowBean wrb = new WeeklyAppointmentRowBean();
            wrb.setDateTime(add.toString().substring(11, 19));
            log.debug(wrb.toString());
            findApps(sunday.toString().substring(0, 10));
            for (AppRecord ar : list) {
                log.info("AppRecord: " + ar.toString());
                int day = getDayNumber(ldtList, ar.getStartTime());
                log.info("Day: " + day);
                log.info("AppRecord start time: " + ar.getStartTime());
                log.info("add: " + add);
                
                switch (day) {
                    case 1:
                        if (ar.getStartTime().equals(add)) {
                            wrb.setSunday(ar.getTitle() + "\n" + ar.getLocation() + "\n" + ar.getStartTime());
                        }
                        if (add.after(ar.getStartTime()) && add.before(ar.getEndTime())) {
                            wrb.setSunday(ar.getTitle() + "\n" + ar.getLocation() + "\n" + ar.getStartTime());
                            try {
                                agrList = agrDAO.findAppGroupRec();
                                for (AppGroupRecord appGroupRec : agrList) {
                                    if (appGroupRec.getId() == ar.getAppGroup()) {
                                        String color = appGroupRec.getColor();
                                        renderCell(sundayColumn, color);
                                    }
                                }
                            } catch (SQLException e) {
                                log.debug(e.getMessage());
                            }
                        }
                        break;
                    case 2:
                            wrb.setMonday(ar.getTitle() + "\n" + ar.getLocation() + "\n" + ar.getStartTime());
                            try {
                                agrList = agrDAO.findAppGroupRec();
                                for (AppGroupRecord appGroupRec : agrList) {
                                    if (appGroupRec.getId() == ar.getAppGroup()) {
                                        String color = appGroupRec.getColor();
                                        renderCell(mondayColumn, color);
                                    }
                                }
                            } catch (SQLException e) {
                                log.debug(e.getMessage());
                            }
                        break;
                    case 3:
                            wrb.setTuesday(ar.getTitle() + "\n" + ar.getLocation() + "\n" + ar.getStartTime());
                            try {
                                agrList = agrDAO.findAppGroupRec();
                                for (AppGroupRecord appGroupRec : agrList) {
                                    if (appGroupRec.getId() == ar.getAppGroup()) {
                                        String color = appGroupRec.getColor();
                                        renderCell(tuesdayColumn, color);
                                    }
                                }
                            } catch (SQLException e) {
                                log.debug(e.getMessage());
                            }
                        break;
                    case 4:
                            wrb.setWednesday(ar.getTitle() + "\n" + ar.getLocation() + "\n" + ar.getStartTime());
                            try {
                                agrList = agrDAO.findAppGroupRec();
                                for (AppGroupRecord appGroupRec : agrList) {
                                    if (appGroupRec.getId() == ar.getAppGroup()) {
                                        String color = appGroupRec.getColor();
                                        renderCell(wednesdayColumn, color);
                                    }
                                }
                            } catch (SQLException e) {
                                log.debug(e.getMessage());
                            }
                        break;
                    case 5:
                            wrb.setThursday(ar.getTitle() + "\n" + ar.getLocation() + "\n" + ar.getStartTime());
                            try {
                                agrList = agrDAO.findAppGroupRec();
                                for (AppGroupRecord appGroupRec : agrList) {
                                    if (appGroupRec.getId() == ar.getAppGroup()) {
                                        String color = appGroupRec.getColor();
                                        renderCell(thursdayColumn, color);
                                    }
                                }
                            } catch (SQLException e) {
                                log.debug(e.getMessage());
                            }
                        break;
                    case 6:
                            wrb.setFriday(ar.getTitle() + "\n" + ar.getLocation() + "\n" + ar.getStartTime());
                            try {
                                agrList = agrDAO.findAppGroupRec();
                                for (AppGroupRecord appGroupRec : agrList) {
                                    if (appGroupRec.getId() == ar.getAppGroup()) {
                                        String color = appGroupRec.getColor();
                                        renderCell(fridayColumn, color);
                                    }
                                }
                            } catch (SQLException e) {
                                log.debug(e.getMessage());
                            }
                        break;
                    case 7:
                            wrb.setSaturday(ar.getTitle() + "\n" + ar.getLocation() + "\n" + ar.getStartTime());
                            try {
                                agrList = agrDAO.findAppGroupRec();
                                for (AppGroupRecord appGroupRec : agrList) {
                                    if (appGroupRec.getId() == ar.getAppGroup()) {
                                        String color = appGroupRec.getColor();
                                        renderCell(saturdayColumn, color);
                                    }
                                }
                            } catch (SQLException e) {
                                log.debug(e.getMessage());
                            }
                        break;
                    default:
                        break;
                }
            }
            rowBean.add(wrb);
            s = s.plusMinutes(30);
        }

        return rowBean;
    }

    /**
     * Returns the day number of the week
     *
     * @param list The list of days in the week
     * @param ts The time to compare
     * @return The day in the week
     */
    private int getDayNumber(List<Timestamp> list, Timestamp ts) {
        int day = 0;

        if (list.get(0).toString().substring(0, 10).equals(ts.toString().substring(0, 10))) {
            day = 1;
        } else if (list.get(1).toString().substring(0, 10).equals(ts.toString().substring(0, 10))) {
            day = 2;
        } else if (list.get(2).toString().substring(0, 10).equals(ts.toString().substring(0, 10))) {
            day = 3;
        } else if (list.get(3).toString().substring(0, 10).equals(ts.toString().substring(0, 10))) {
            day = 4;
        } else if (list.get(4).toString().substring(0, 10).equals(ts.toString().substring(0, 10))) {
            day = 5;
        } else if (list.get(5).toString().substring(0, 10).equals(ts.toString().substring(0, 10))) {
            day = 6;
        } else if (list.get(6).toString().substring(0, 10).equals(ts.toString().substring(0, 10))) {
            day = 7;
        }
        return day;
    }

    /**
     * Displays single cell info
     *
     * @param change
     */
    private void showSingleCellDetails(ListChangeListener.Change<? extends TablePosition> change) {
        if (theCells.size() > 0) {
            TablePosition selectedCell = theCells.get(0);
            TableColumn column = selectedCell.getTableColumn();
            int rowIndex = selectedCell.getRow();
            Object data = column.getCellObservableValue(rowIndex).getValue();
            log.debug("data: " + data);
            try {
                if (!data.toString().equals("")) {
                    dialogSingleCellDetails((String) data);
                } else {
                    dialogSingleCellDetailsEmpty();
                }
            } catch (IOException ex) {
                log.debug("Exception: " + ex);
            }
        }
    }

    /**
     * Display the string contents of a single cell.
     *
     * @param data
     */
    private void dialogSingleCellDetails(String data) throws IOException {
        log.debug("In AppRec Form");

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AppRecordFormFXML.fxml"));
        loader.setResources(ResourceBundle.getBundle("MessageBundle"));
        Parent root = (DialogPane) loader.load();
        AppRecordFormFXMLController controller = loader.getController();
        controller.setAppRecDAOData(data.substring(data.indexOf("20")));
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.show();
    }

    /**
     * Display the string contents of a single cell.
     *
     * @param data
     */
    private void dialogSingleCellDetailsEmpty() throws IOException {
        log.debug("In AppRec Form Empty");

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AppRecordFormFXML.fxml"));
        loader.setResources(ResourceBundle.getBundle("MessageBundle"));
        Parent root = (DialogPane) loader.load();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.show();
    }

    /**
     * Finds all appointments for the month
     */
    public void findApps(String day) {
        try {
            this.list = arDAO.findAppForWeek(day);
            for (AppRecord ar : list) {
                log.info(ar.toString());
            }
        } catch (SQLException ex) {
            log.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    /**
     * Error pop up message
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(ResourceBundle.getBundle("MessageBundle").getString("ERRORDAO"));
        dialog.setHeaderText(ResourceBundle.getBundle("MessageBundle").getString("ERRORDAOTEXT"));
        dialog.setContentText(msg);
        dialog.show();
    }

    /**
     * Responsible for the contents of a cell to update the color of a cell
     *
     * @param tc
     */
    private void renderCell(TableColumn tc, String color) {
        tc.setCellFactory(column -> {
            return new TableCell<DailyAppointmentRowBean, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(item);
                    log.info("ITEM: " + item);
                    if (item == null || empty || item.equals("")) {
                        setText(null);
                        setStyle("");
                    } else {
                        setStyle("-fx-background-color: " + color + ";" + "-fx-text-fill: white;");
                        if (color.equals("#FFA500") || color.equals("#ADD8E6")) {
                            setStyle("-fx-background-color: " + color + ";" + "-fx-text-fill: white;");
                        }
                    }
                }
            };
        });
    }
}
