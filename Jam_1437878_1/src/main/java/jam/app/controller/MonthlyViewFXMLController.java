package jam.app.controller;

import jam.business.AppGroupRecImpl;
import jam.business.AppRecImpl;
import jam.business.IAppGroupRecDAO;
import jam.business.IAppRecDAO;
import jam.entities.AppGroupRecord;
import jam.entities.AppRecord;
import jam.entities.Configuration;
import jam.entities.DailyAppointmentRowBean;
import jam.entities.MonthRowBean;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FXML Controller class
 *
 * @author Kevin
 */
public class MonthlyViewFXMLController {

    private final Logger log = LoggerFactory.getLogger(this.getClass()
            .getName());
    private IAppRecDAO arDAO;
    private IAppGroupRecDAO agrDAO;
    private List<AppRecord> list;
    private ObservableList<TablePosition> theCells;
    private int month;
    private int year;
    private String day;
    private int monthCounter = 0;
    private Configuration config;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TableView<MonthRowBean> monthlyView;

    @FXML
    private Label title;

    @FXML
    private TableColumn<MonthRowBean, String> sundayColumn;

    @FXML
    private TableColumn<MonthRowBean, String> mondayColumn;

    @FXML
    private TableColumn<MonthRowBean, String> tuesdayColumn;

    @FXML
    private TableColumn<MonthRowBean, String> wednesdayColumn;

    @FXML
    private TableColumn<MonthRowBean, String> thursdayColumn;

    @FXML
    private TableColumn<MonthRowBean, String> fridayColumn;

    @FXML
    private TableColumn<MonthRowBean, String> saturdayColumn;

    @FXML
    private Button previousBtn;

    @FXML
    private Button nextBtn;

    /**
     * Default constructor
     */
    public MonthlyViewFXMLController() {
        super();
        setUp();
        this.arDAO = new AppRecImpl(config);
        this.agrDAO = new AppGroupRecImpl(config);
        log.info("Default Constructor");
    }

    /**
     * Sets up the Configuration
     */
    public void setUp() {
        config = new Configuration();
        config.setUserName("kevin");
        config.setPassword("jampassword");
        config.setUrl("jdbc:mysql://localhost:3306/jamdb");
    }

    /**
     * Set the title of the view
     *
     * @param text
     */
    public void setTitle(String text) {
        this.title.setText(text);
    }

    /**
     * Get the month string representation
     *
     * @return The month string representation
     */
    public String getMonth() {
        return getMonthStr();
    }

    @FXML
    private void showNextMonth(ActionEvent event) throws SQLException {
        monthCounter--;
        month++;
        if (month == 13) {
            month = 1;
        } else if (month == -1) {
            month = 12;
        }
        this.title.setText(getMonth());
        monthlyView.setItems(fillViewMonth());
    }

    @FXML
    private void showPreviousMonth(ActionEvent event) throws SQLException {
        monthCounter++;
        month--;
        if (month == 13) {
            month = 1;
        } else if (month == -1) {
            month = 12;
        }
        this.title.setText(getMonth());
        monthlyView.setItems(fillViewMonth());
    }

    @FXML
    private void initialize() throws SQLException {
        sundayColumn.setCellValueFactory(cellData -> cellData.getValue()
                .col1Property());
        mondayColumn.setCellValueFactory(cellData -> cellData.getValue()
                .col2Property());
        tuesdayColumn.setCellValueFactory(cellData -> cellData.getValue()
                .col3Property());
        wednesdayColumn.setCellValueFactory(cellData -> cellData.getValue()
                .col4Property());
        thursdayColumn.setCellValueFactory(cellData -> cellData.getValue()
                .col5Property());
        fridayColumn.setCellValueFactory(cellData -> cellData.getValue()
                .col6Property());
        saturdayColumn.setCellValueFactory(cellData -> cellData.getValue()
                .col7Property());

        adjustColumnWidths();
        monthlyView.getSelectionModel().setCellSelectionEnabled(true);

        theCells = monthlyView.getSelectionModel().getSelectedCells();
        theCells.addListener(this::showSingleCellDetails);
        LocalDateTime ldt = LocalDateTime.now().minusMonths(monthCounter);
        month = ldt.getMonthValue();
        year = ldt.getYear();
        displayTheTable();
    }

    /**
     * Adjust the column width
     */
    private void adjustColumnWidths() {
        // Get the current width of the table
        double width = monthlyView.getPrefWidth();
        // Set width of each column
        sundayColumn.setPrefWidth(width / 7.0);
        mondayColumn.setPrefWidth(width / 7.0);
        tuesdayColumn.setPrefWidth(width / 7.0);
        wednesdayColumn.setPrefWidth(width / 7.0);
        thursdayColumn.setPrefWidth(width / 7.0);
        fridayColumn.setPrefWidth(width / 7.0);
        saturdayColumn.setPrefWidth(width / 7.0);
        monthlyView.setFixedCellSize(width / 7.0);
    }

    /**
     * Displays the monthly view
     *
     * @throws SQLException
     */
    public void displayTheTable() throws SQLException {
        monthlyView.setItems(fillViewMonth());
    }

    /**
     * Displays the month view
     *
     * @return ObservableList of MonthRowBean
     */
    private ObservableList<MonthRowBean> fillViewMonth() throws SQLException {
        List<AppGroupRecord> agrList = agrDAO.findAppGroupRec();
        String color = "";
        int date = 0;
        findApps();
        for (AppRecord ar : list) {
            log.info(ar.toString());
        }

        LocalDateTime now = LocalDateTime.now().minusMonths(monthCounter);
        if (month == 13) {
            monthCounter = 1;
        } else if (month == -1) {
            monthCounter = 12;
        }
        month = now.getMonthValue();
        day = now.withDayOfMonth(1).getDayOfWeek().name();
        int dayCounter = 1;
        ObservableList<MonthRowBean> rowBean = FXCollections.observableArrayList();

        // Welcome to the world record biggest for loop in human history
        for (int i = 0; i <= 5; i++) {
            MonthRowBean mrb = new MonthRowBean();
            for (int j = (i == 0) ? getDayNumber() : 0; j < 7; j++) {
                log.debug("j is " + j + " dayCounter: " + dayCounter);
                switch (j) {
                    case 0:
                        switch (month) {
                            case 1:
                            case 3:
                            case 5:
                            case 7:
                            case 8:
                            case 10:
                            case 12:
                                if (dayCounter > 31) {
                                    mrb.setCol1("");
                                } else {
                                    for (AppRecord ar : list) {
                                        date = getDate(ar);
                                        if (date == dayCounter) {
                                            log.info("Date was equal to dayCounter: " + date + " / " + dayCounter);
                                            mrb.setAppointment(ar);
                                            mrb.setCol1(dayCounter + "\n" + ar.getTitle() + "\n" + ar.getStartTime().toString());
                                            for (AppGroupRecord agr : agrList) {
                                                checkCellAndRender(agr, ar, sundayColumn);
                                            }
                                        } else {
                                            if (mrb.getCol1().isEmpty()) {
                                                mrb.setCol1(dayCounter + "");
                                            }
                                        } // End else
                                    }
                                }
                                break;
                            case 2:
                                if (dayCounter > 28) {
                                    mrb.setCol1("");
                                } else {
                                    for (AppRecord ar : list) {
                                        date = getDate(ar);
                                        if (date == dayCounter) {
                                            log.info("Date was equal to dayCounter: " + date + " / " + dayCounter);
                                            mrb.setAppointment(ar);
                                            mrb.setCol1(dayCounter + "\n" + ar.getTitle() + "\n" + ar.getStartTime().toString());
                                            for (AppGroupRecord agr : agrList) {
                                                checkCellAndRender(agr, ar, sundayColumn);
                                            }
                                        } else if (mrb.getCol1().isEmpty()) {
                                            mrb.setCol1(dayCounter + "");
                                        } // End else
                                    }
                                }
                                break;
                            case 4:
                            case 6:
                            case 9:
                            case 11:
                                if (dayCounter > 30) {
                                    mrb.setCol1("");
                                } else {
                                    for (AppRecord ar : list) {
                                        date = getDate(ar);
                                        if (date == dayCounter) {
                                            log.info("Date was equal to dayCounter: " + date + " / " + dayCounter);
                                            mrb.setAppointment(ar);
                                            mrb.setCol1(dayCounter + "\n" + ar.getTitle() + "\n" + ar.getStartTime().toString());
                                            for (AppGroupRecord agr : agrList) {
                                                checkCellAndRender(agr, ar, sundayColumn);
                                            }
                                        } else {
                                            if (mrb.getCol1().isEmpty()) {
                                                mrb.setCol1(dayCounter + "");
                                            }
                                        }
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                        break;

                    case 1:
                        switch (month) {
                            case 1:
                            case 3:
                            case 5:
                            case 7:
                            case 8:
                            case 10:
                            case 12:
                                if (dayCounter > 31) {
                                    mrb.setCol2("");
                                } else {
                                    for (AppRecord ar : list) {
                                        date = getDate(ar);
                                        if (date == dayCounter) {
                                            log.info("Date was equal to dayCounter: " + date + " / " + dayCounter);
                                            mrb.setAppointment(ar);
                                            mrb.setCol2(dayCounter + "\n" + ar.getTitle() + "\n" + ar.getStartTime().toString());
                                            for (AppGroupRecord agr : agrList) {
                                                checkCellAndRender(agr, ar, mondayColumn);
                                            }
                                        } else if (mrb.getCol2().isEmpty()) {
                                            mrb.setCol2(dayCounter + "");
                                        } // End else
                                    }
                                }
                                break;
                            case 2:
                                if (dayCounter > 28) {
                                    mrb.setCol2("");
                                } else {
                                    for (AppRecord ar : list) {
                                        date = getDate(ar);
                                        if (date == dayCounter) {
                                            log.info("Date was equal to dayCounter: " + date + " / " + dayCounter);
                                            mrb.setAppointment(ar);
                                            mrb.setCol2(dayCounter + "\n" + ar.getTitle() + "\n" + ar.getStartTime().toString());
                                            for (AppGroupRecord agr : agrList) {
                                                checkCellAndRender(agr, ar, mondayColumn);
                                            }
                                        } else {
                                            if (mrb.getCol2().isEmpty()) {
                                                mrb.setCol2(dayCounter + "");
                                            }
                                        } // End else
                                    }
                                }
                                break;
                            case 4:
                            case 6:
                            case 9:
                            case 11:
                                if (dayCounter > 30) {
                                    mrb.setCol2("");
                                } else {
                                    for (AppRecord ar : list) {
                                        date = getDate(ar);
                                        if (date == dayCounter) {
                                            log.info("Date was equal to dayCounter: " + date + " / " + dayCounter);
                                            mrb.setAppointment(ar);
                                            mrb.setCol2(dayCounter + "\n" + ar.getTitle() + "\n" + ar.getStartTime().toString());
                                            for (AppGroupRecord agr : agrList) {
                                                checkCellAndRender(agr, ar, mondayColumn);
                                            }
                                        } else {
                                            if (mrb.getCol2().isEmpty()) {
                                                mrb.setCol2(dayCounter + "");
                                            }
                                        } // End else
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                    case 2:
                        switch (month) {
                            case 1:
                            case 3:
                            case 5:
                            case 7:
                            case 8:
                            case 10:
                            case 12:
                                if (dayCounter > 31) {
                                    mrb.setCol3("");
                                } else {
                                    for (AppRecord ar : list) {
                                        date = getDate(ar);
                                        if (date == dayCounter) {
                                            log.info("Date was equal to dayCounter: " + date + " / " + dayCounter);
                                            mrb.setAppointment(ar);
                                            mrb.setCol3(dayCounter + "\n" + ar.getTitle() + "\n" + ar.getStartTime().toString());
                                            for (AppGroupRecord agr : agrList) {
                                                checkCellAndRender(agr, ar, tuesdayColumn);
                                            }
                                        } else {
                                            if (mrb.getCol3().isEmpty()) {
                                                mrb.setCol3(dayCounter + "");
                                            }
                                        } // End else
                                    }
                                }
                                break;
                            case 2:
                                if (dayCounter > 28) {
                                    mrb.setCol3("");
                                } else {
                                    for (AppRecord ar : list) {
                                        date = getDate(ar);
                                        if (date == dayCounter) {
                                            log.info("Date was equal to dayCounter: " + date + " / " + dayCounter);
                                            mrb.setAppointment(ar);
                                            mrb.setCol3(dayCounter + "\n" + ar.getTitle() + "\n" + ar.getStartTime().toString());
                                            for (AppGroupRecord agr : agrList) {
                                                checkCellAndRender(agr, ar, tuesdayColumn);
                                            }
                                        } else {
                                            if (mrb.getCol3().isEmpty()) {
                                                mrb.setCol3(dayCounter + "");
                                            }
                                        } // End else
                                    }
                                }
                                break;
                            case 4:
                            case 6:
                            case 9:
                            case 11:
                                if (dayCounter > 30) {
                                    mrb.setCol3("");
                                } else {
                                    for (AppRecord ar : list) {
                                        date = getDate(ar);
                                        if (date == dayCounter) {
                                            log.info("Date was equal to dayCounter: " + date + " / " + dayCounter);
                                            mrb.setAppointment(ar);
                                            mrb.setCol3(dayCounter + "\n" + ar.getTitle() + "\n" + ar.getStartTime().toString());
                                            for (AppGroupRecord agr : agrList) {
                                                checkCellAndRender(agr, ar, tuesdayColumn);
                                            }
                                        } else {
                                            if (mrb.getCol3().isEmpty()) {
                                                mrb.setCol3(dayCounter + "");
                                            }
                                        } // End else
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                    case 3:
                        switch (month) {
                            case 1:
                            case 3:
                            case 5:
                            case 7:
                            case 8:
                            case 10:
                            case 12:
                                if (dayCounter > 31) {
                                    mrb.setCol4("");
                                } else {
                                    for (AppRecord ar : list) {
                                        date = getDate(ar);
                                        if (date == dayCounter) {
                                            log.info("Date was equal to dayCounter: " + date + " / " + dayCounter);
                                            mrb.setAppointment(ar);
                                            mrb.setCol4(dayCounter + "\n" + ar.getTitle() + "\n" + ar.getStartTime().toString());
                                            for (AppGroupRecord agr : agrList) {
                                                checkCellAndRender(agr, ar, wednesdayColumn);
                                            }
                                        } else {
                                            if (mrb.getCol4().isEmpty()) {
                                                mrb.setCol4(dayCounter + "");
                                            }
                                        } // End else
                                    }
                                }
                                break;
                            case 2:
                                if (dayCounter > 28) {
                                    mrb.setCol4("");
                                } else {
                                    for (AppRecord ar : list) {
                                        date = getDate(ar);
                                        if (date == dayCounter) {
                                            log.info("Date was equal to dayCounter: " + date + " / " + dayCounter);
                                            mrb.setAppointment(ar);
                                            mrb.setCol4(dayCounter + "\n" + ar.getTitle() + "\n" + ar.getStartTime().toString());
                                            for (AppGroupRecord agr : agrList) {
                                                checkCellAndRender(agr, ar, wednesdayColumn);
                                            }
                                        } else {
                                            if (mrb.getCol4().isEmpty()) {
                                                mrb.setCol4(dayCounter + "");
                                            }
                                        } // End else
                                    }
                                }
                                break;
                            case 4:
                            case 6:
                            case 9:
                            case 11:
                                if (dayCounter > 30) {
                                    mrb.setCol4("");
                                } else {
                                    for (AppRecord ar : list) {
                                        date = getDate(ar);
                                        if (date == dayCounter) {
                                            log.info("Date was equal to dayCounter: " + date + " / " + dayCounter);
                                            mrb.setAppointment(ar);
                                            mrb.setCol4(dayCounter + "\n" + ar.getTitle() + "\n" + ar.getStartTime().toString());
                                            for (AppGroupRecord agr : agrList) {
                                                checkCellAndRender(agr, ar, wednesdayColumn);
                                            }
                                        } else {
                                            if (mrb.getCol4().isEmpty()) {
                                                mrb.setCol4(dayCounter + "");
                                            }
                                        } // End else
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                    case 4:
                        switch (month) {
                            case 1:
                            case 3:
                            case 5:
                            case 7:
                            case 8:
                            case 10:
                            case 12:
                                if (dayCounter > 31) {
                                    mrb.setCol5("");
                                } else {
                                    for (AppRecord ar : list) {
                                        date = getDate(ar);
                                        if (date == dayCounter) {
                                            log.info("Date was equal to dayCounter: " + date + " / " + dayCounter);
                                            mrb.setAppointment(ar);
                                            mrb.setCol5(dayCounter + "\n" + ar.getTitle() + "\n" + ar.getStartTime().toString());
                                            for (AppGroupRecord agr : agrList) {
                                                checkCellAndRender(agr, ar, thursdayColumn);
                                            }
                                        } else {
                                            if (mrb.getCol5().isEmpty()) {
                                                mrb.setCol5(dayCounter + "");
                                            }
                                        } // End else
                                    }
                                }
                                break;
                            case 2:
                                if (dayCounter > 28) {
                                    mrb.setCol5("");
                                } else {
                                    for (AppRecord ar : list) {
                                        date = getDate(ar);
                                        if (date == dayCounter) {
                                            log.info("Date was equal to dayCounter: " + date + " / " + dayCounter);
                                            mrb.setAppointment(ar);
                                            mrb.setCol5(dayCounter + "\n" + ar.getTitle() + "\n" + ar.getStartTime().toString());
                                            for (AppGroupRecord agr : agrList) {
                                                checkCellAndRender(agr, ar, thursdayColumn);
                                            }
                                        } else {
                                            if (mrb.getCol5().isEmpty()) {
                                                mrb.setCol5(dayCounter + "");
                                            }
                                        } // End else
                                    }
                                }
                                break;
                            case 4:
                            case 6:
                            case 9:
                            case 11:
                                if (dayCounter > 30) {
                                    mrb.setCol5("");
                                } else {
                                    for (AppRecord ar : list) {
                                        date = getDate(ar);
                                        if (date == dayCounter) {
                                            log.info("Date was equal to dayCounter: " + date + " / " + dayCounter);
                                            mrb.setAppointment(ar);
                                            mrb.setCol5(dayCounter + "\n" + ar.getTitle() + "\n" + ar.getStartTime().toString());
                                            for (AppGroupRecord agr : agrList) {
                                                checkCellAndRender(agr, ar, thursdayColumn);
                                            }
                                        } else {
                                            if (mrb.getCol5().isEmpty()) {
                                                mrb.setCol5(dayCounter + "");
                                            }
                                        } // End else
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                    case 5:
                        switch (month) {
                            case 1:
                            case 3:
                            case 5:
                            case 7:
                            case 8:
                            case 10:
                            case 12:
                                if (dayCounter > 31) {
                                    mrb.setCol6("");
                                } else {
                                    for (AppRecord ar : list) {
                                        date = getDate(ar);
                                        if (date == dayCounter) {
                                            log.info("Date was equal to dayCounter: " + date + " / " + dayCounter);
                                            mrb.setAppointment(ar);
                                            mrb.setCol6(dayCounter + "\n" + ar.getTitle() + "\n" + ar.getStartTime().toString());
                                            for (AppGroupRecord agr : agrList) {
                                                checkCellAndRender(agr, ar, fridayColumn);
                                            }
                                        } else {
                                            if (mrb.getCol6().isEmpty()) {
                                                mrb.setCol6(dayCounter + "");
                                            }
                                        } // End else
                                    }
                                }
                                break;
                            case 2:
                                if (dayCounter > 28) {
                                    mrb.setCol6("");
                                } else {
                                    for (AppRecord ar : list) {
                                        date = getDate(ar);
                                        if (date == dayCounter) {
                                            log.info("Date was equal to dayCounter: " + date + " / " + dayCounter);
                                            mrb.setAppointment(ar);
                                            mrb.setCol6(dayCounter + "\n" + ar.getTitle() + "\n" + ar.getStartTime().toString());
                                            for (AppGroupRecord agr : agrList) {
                                                checkCellAndRender(agr, ar, fridayColumn);
                                            }
                                        } else {
                                            if (mrb.getCol6().isEmpty()) {
                                                mrb.setCol6(dayCounter + "");
                                            }
                                        } // End else
                                    }
                                }
                                break;
                            case 4:
                            case 6:
                            case 9:
                            case 11:
                                if (dayCounter > 30) {
                                    mrb.setCol6("");
                                } else {
                                    for (AppRecord ar : list) {
                                        date = getDate(ar);
                                        if (date == dayCounter) {
                                            log.info("Date was equal to dayCounter: " + date + " / " + dayCounter);
                                            mrb.setAppointment(ar);
                                            mrb.setCol6(dayCounter + "\n" + ar.getTitle() + "\n" + ar.getStartTime().toString());
                                            for (AppGroupRecord agr : agrList) {
                                                checkCellAndRender(agr, ar, fridayColumn);
                                            }
                                        } else {
                                            if (mrb.getCol6().isEmpty()) {
                                                mrb.setCol6(dayCounter + "");
                                            }
                                        } // End else
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                    case 6:
                        switch (month) {
                            case 1:
                            case 3:
                            case 5:
                            case 7:
                            case 8:
                            case 10:
                            case 12:
                                if (dayCounter > 31) {
                                    mrb.setCol7("");
                                } else {
                                    for (AppRecord ar : list) {
                                        date = getDate(ar);
                                        if (date == dayCounter) {
                                            log.info("Date was equal to dayCounter: " + date + " / " + dayCounter);
                                            mrb.setAppointment(ar);
                                            mrb.setCol7(dayCounter + "\n" + ar.getTitle() + "\n" + ar.getStartTime().toString());
                                            for (AppGroupRecord agr : agrList) {
                                                checkCellAndRender(agr, ar, saturdayColumn);
                                            }
                                        } else {
                                            if (mrb.getCol7().isEmpty()) {
                                                mrb.setCol7(dayCounter + "");
                                            }
                                        } // End else
                                    }
                                }
                                break;
                            case 2:
                                if (dayCounter > 28) {
                                    mrb.setCol7("");
                                } else {
                                    for (AppRecord ar : list) {
                                        date = getDate(ar);
                                        if (date == dayCounter) {
                                            log.info("Date was equal to dayCounter: " + date + " / " + dayCounter);
                                            mrb.setAppointment(ar);
                                            mrb.setCol7(dayCounter + "\n" + ar.getTitle() + "\n" + ar.getStartTime().toString());
                                            for (AppGroupRecord agr : agrList) {
                                                checkCellAndRender(agr, ar, saturdayColumn);
                                            }
                                        } else {
                                            if (mrb.getCol7().isEmpty()) {
                                                mrb.setCol7(dayCounter + "");
                                            }
                                        } // End else
                                    }
                                }
                                break;
                            case 4:
                            case 6:
                            case 9:
                            case 11:
                                if (dayCounter > 30) {
                                    mrb.setCol7("");
                                } else {
                                    for (AppRecord ar : list) {
                                        date = getDate(ar);
                                        if (date == dayCounter) {
                                            log.info("Date was equal to dayCounter: " + date + " / " + dayCounter);
                                            mrb.setAppointment(ar);
                                            mrb.setCol7(dayCounter + "\n" + ar.getTitle() + "\n" + ar.getStartTime().toString());
                                            for (AppGroupRecord agr : agrList) {
                                                checkCellAndRender(agr, ar, saturdayColumn);
                                            }
                                        } else {
                                            if (mrb.getCol7().isEmpty()) {
                                                mrb.setCol7(dayCounter + "");
                                            }
                                        } // End else
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                }
                dayCounter++;
            }
            rowBean.add(mrb);
        }

        return rowBean;
    }

    /**
     * Checks if rendering is necessary
     *
     * @param agr The appointment group record
     * @param ar The appointment record
     * @param tc The table column
     */
    private void checkCellAndRender(AppGroupRecord agr, AppRecord ar, TableColumn tc) {
        if (agr.getId() == ar.getAppGroup()) {
            AppRecord appColor = ar;
            renderCell(tc, appColor);
        }
    }

    /**
     * Returns the day of the month from the appointment record
     *
     * @param ar The appointment record
     * @return The date of the appointment record
     */
    private int getDate(AppRecord ar) {
        int date = 0;
        LocalDateTime ldt = ar.getStartTime().toLocalDateTime();
        date = ldt.getDayOfMonth();
        return date;
    }

    /**
     * Returns the first day of the month position
     *
     * @return The first day of the month
     */
    private int getDayNumber() {
        int j = 0;
        switch (day) {
            case "SUNDAY":
                j = 0;
                break;
            case "MONDAY":
                j = 1;
                break;
            case "TUESDAY":
                j = 2;
                break;
            case "WEDNESDAY":
                j = 3;
                break;
            case "THURSDAY":
                j = 4;
                break;
            case "FRIDAY":
                j = 5;
                break;
            case "SATURDAY":
                j = 6;
                break;
        }
        return j;
    }

    /**
     * Displays single cell info
     *
     * @param change
     */
    private void showSingleCellDetails(ListChangeListener.Change<? extends TablePosition> change) {
        if (theCells.size() > 0) {
            TablePosition selectedCell = theCells.get(0);
            TableColumn column = selectedCell.getTableColumn();
            int rowIndex = selectedCell.getRow();
            Object data = column.getCellObservableValue(rowIndex).getValue();
            log.debug("data: " + data);
            try {
                if (data.toString().length() > 2) {
                    dialogSingleCellDetails((String) data);
                } else {
                    dialogSingleCellDetailsEmpty();
                }
            } catch (IOException ex) {
                log.debug("Exception: " + ex);
            }
        }
    }

    /**
     * Display the string contents of a single cell.
     *
     * @param data
     */
    private void dialogSingleCellDetails(String data) throws IOException {
        log.debug("In AppRec Form");

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AppRecordFormFXML.fxml"));
        loader.setResources(ResourceBundle.getBundle("MessageBundle"));
        Parent root = (DialogPane) loader.load();
        AppRecordFormFXMLController controller = loader.getController();
        controller.setAppRecDAOData(data.substring(data.indexOf("20")));
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.show();
    }

    /**
     * Display the string contents of a single cell.
     *
     * @param data
     */
    private void dialogSingleCellDetailsEmpty() throws IOException {
        log.debug("In AppRec Form Empty");

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AppRecordFormFXML.fxml"));
        loader.setResources(ResourceBundle.getBundle("MessageBundle"));
        Parent root = (DialogPane) loader.load();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.show();
    }

    /**
     * Gets the string representation of the month
     *
     * @return The month
     */
    private String getMonthStr() {
        String m = "";
        switch (month) {
            case 1:
                m = "January";
                break;
            case 2:
                m = "February";
                break;
            case 3:
                m = "March";
                break;
            case 4:
                m = "April";
                break;
            case 5:
                m = "May";
                break;
            case 6:
                m = "June";
                break;
            case 7:
                m = "July";
                break;
            case 8:
                m = "August";
                break;
            case 9:
                m = "September";
                break;
            case 10:
                m = "October";
                break;
            case 11:
                m = "November";
                break;
            case 12:
                m = "December";
                break;
        }
        return m;
    }

    /**
     * Finds all appointments for the month
     */
    public void findApps() {
        try {
            log.info("Month is: " + getMonthStr());
            list = arDAO.findAppForMonth(getMonthStr());
        } catch (SQLException ex) {
            log.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    /**
     * Error pop up message
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(ResourceBundle.getBundle("MessageBundle").getString("ERRORDAO"));
        dialog.setHeaderText(ResourceBundle.getBundle("MessageBundle").getString("ERRORDAOTEXT"));
        dialog.setContentText(msg);
        dialog.show();
    }

    /**
     * Responsible for the contents of a cell to update the color of a cell
     *
     * @param tc
     */
    private void renderCell(TableColumn tc, AppRecord ar) {
        tc.setCellFactory(column -> {
            return new TableCell<DailyAppointmentRowBean, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(item);
                    log.info("ITEM: " + item);

                    if (item == null || empty || item.equals("") || item.length() == 1 || item.length() == 2) {
                        setStyle("");
                    } else {
                        try {
                            List<AppGroupRecord> agrList = agrDAO.findAppGroupRec();
                            for (AppGroupRecord agr : agrList) {
                                if (agr.getId() == ar.getAppGroup()) {
                                    String color = agr.getColor();
                                    setStyle("-fx-background-color: " + color + ";" + "-fx-text-fill: white;");
                                    if (color.equals("#FFA500") || color.equals("#ADD8E6")) {
                                        setStyle("-fx-background-color: " + color + ";" + "-fx-text-fill: white;");
                                    }
                                }
                            }
                        } catch (SQLException e) {
                            log.debug(e.getMessage());
                        }

                    }
                }
            };
        });
    }
}
