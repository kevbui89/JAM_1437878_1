package jam.app.controller;

import jam.business.AppRecImpl;
import jam.business.IAppRecDAO;
import jam.entities.AppRecord;
import jam.entities.Configuration;
import java.net.URL;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import javafx.beans.Observable;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.util.converter.NumberStringConverter;
import org.slf4j.LoggerFactory;

/**
 * Appointment Record Form
 *
 * @author Kevin
 */
public class AppRecordFormFXMLController {

    private final org.slf4j.Logger log = LoggerFactory.getLogger(this.getClass().getName());
    private IAppRecDAO arDAO;
    private AppRecord ar;
    private Configuration config;
    private List<AppRecord> list;
    private int appCounter;
    private int maxApp = 0;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField appRecLocationTxtfield;

    @FXML
    private TextField appRecIdTxtfield;

    @FXML
    private TextField appRecTitleTxtfield;

    @FXML
    private TextField appRecDetailsTxtfield;

    @FXML
    private TextField appRecWholedayTxtfield;

    @FXML
    private TextField appRecAppGroupTxtfield;

    @FXML
    private TextField appRecAlarmTxtfield;

    @FXML
    private TextField startTimeTxtfield;

    @FXML
    private TextField endTimeTxtfield;

    @FXML
    private Button appRecAddBtn;

    @FXML
    private Button appRecDeleteBtn;

    @FXML
    private Button appRecSaveBtn;

    @FXML
    private Button appRecClearBtn;

    @FXML
    private Button appRecPreviousBtn;

    @FXML
    private Button appRecNextBtn;

    @FXML
    private DatePicker startPicker;

    @FXML
    private DatePicker endPicker;

    /**
     * Default constructor
     */
    public AppRecordFormFXMLController() {
        super();
        setUp();
        this.arDAO = new AppRecImpl(config);
        this.ar = new AppRecord();
        this.appCounter = 0;
        log.info("Default Constructor");
    }

    /**
     * Sets up the Configuration
     */
    public void setUp() {
        config = new Configuration();
        config.setUserName("kevin");
        config.setPassword("jampassword");
        config.setUrl("jdbc:mysql://localhost:3306/jamdb");
    }

    private void doBindings() {
        Bindings.bindBidirectional(appRecIdTxtfield.textProperty(), ar.idProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(appRecTitleTxtfield.textProperty(), ar.titleProperty());
        Bindings.bindBidirectional(appRecLocationTxtfield.textProperty(), ar.locationProperty());
        Bindings.bindBidirectional(startPicker.valueProperty(), ar.dateStartFieldProperty());
        Bindings.bindBidirectional(startTimeTxtfield.textProperty(), ar.StartTimeFormProperty());
        Bindings.bindBidirectional(endPicker.valueProperty(), ar.dateEndFieldProperty());
        Bindings.bindBidirectional(endTimeTxtfield.textProperty(), ar.EndTimeFormProperty());
        Bindings.bindBidirectional(appRecDetailsTxtfield.textProperty(), ar.detailsProperty());
        Bindings.bindBidirectional(appRecWholedayTxtfield.textProperty(), ar.wholeDayProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(appRecAppGroupTxtfield.textProperty(), ar.groupProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(appRecAlarmTxtfield.textProperty(), ar.alarmProperty(), new NumberStringConverter());
    }

    @FXML
    void addAppRec(ActionEvent event) {
        doBindings();
        log.debug("Appointment Record to create: " + ar);
        try {
            arDAO.createAppRec(ar);
        } catch (SQLException ex) {
            log.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    @FXML
    void deleteAppRec(ActionEvent event) {
        doBindings();
        log.debug("Appointment Record to delete: " + ar);
        try {
            arDAO.deleteAppRec(ar);
        } catch (SQLException ex) {
            log.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    @FXML
    void clearAppRec(ActionEvent event) {
        ar.setAppId(0);
        ar.setTitle("");
        ar.setLocation("");
        ar.setStartTime(null);
        ar.setEndTime(null);
        ar.setDetails("");
        ar.setWholeDay(0);
        ar.setAlarm(0);
        ar.SetAppGroup(0);
    }

    @FXML
    void nextAppRec(ActionEvent event) {
        if (!list.isEmpty()) {
            appCounter++;
            log.info("appCounter: " + appCounter);
            log.info("maxApp: " + maxApp);
            if (appCounter >= maxApp) {
                ar = list.get(appCounter);
                doBindings();
                appCounter = -1;
            } else {
                ar = list.get(appCounter);
                doBindings();
            }
        }
    }

    @FXML
    void previousAppRec(ActionEvent event) {
        if (!list.isEmpty()) {
            appCounter--;
            log.info("appCounter: " + appCounter);
            log.info("maxApp: " + maxApp);
            if (appCounter <= -1) {
                appCounter = maxApp;
                ar = list.get(appCounter);
                doBindings();
            } else {
                ar = list.get(appCounter);
                doBindings();
            }
        }
    }

    @FXML
    void saveAppRec(ActionEvent event) {
        doBindings();
        log.debug("Appointment Record to update: " + ar);
        try {
            arDAO.updateAppRec(ar);
        } catch (SQLException ex) {
            log.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    @FXML
    void initialize() {
        appRecIdTxtfield.setEditable(false);
        appRecAlarmTxtfield.focusedProperty().addListener(this::listenForAlarm);
        appRecWholedayTxtfield.focusedProperty().addListener(this::listenForWholeDay);

    }

    /**
     * Sets up the appointment record from the timestamp
     * @param ts The timestamp value
     */
    public void setAppRecDAOData(String ts) {
        log.debug("Timestamp is " + ts);
        try {
            this.ar = arDAO.findApp(Timestamp.valueOf(ts));
            if (this.ar != null) {
                doBindings();
            } 
        } catch (SQLException ex) {
            log.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }
    
    /**
     * Sets up the appointment record from the timestamp
     * @param ar The appointment record value
     */
    public void setAppSearch(AppRecord ar) {
        log.debug("AppRecord is " + ar);
        try {
            this.list = arDAO.getAppFromTitle(ar);
            this.maxApp = list.size() - 1;
            if (!list.isEmpty()) {
                this.ar = list.get(0);
                doBindings();
            } else {
                errorAlert("No appointments with that title");
            }
        } catch (SQLException ex) {
            log.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    /**
     * Error pop up message
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(ResourceBundle.getBundle("MessageBundle").getString("ERRORDAO"));
        dialog.setHeaderText(ResourceBundle.getBundle("MessageBundle").getString("ERRORDAOTEXT"));
        dialog.setContentText(msg);
        dialog.show();
    }

    /**
     * Listens to check if the alarm is valid
     *
     * @param obs
     */
    private void listenForAlarm(Observable obs) {
        if (!appRecAlarmTxtfield.isFocused()) {
            String alarm = appRecAlarmTxtfield.getText();
            int alarmInt = Integer.parseInt(alarm);
            if (alarmInt < 0 || alarmInt > 1) {
                appRecAlarmTxtfield.setText("Invalid Alarm - Please set a valid alarm (0 for false, 1 for true");
            }
            log.info("Lost focus");
        } else {
            log.info("Gained Focus");
        }
    }

    /**
     * Listens to check if the whole day option is valid
     *
     * @param obs
     */
    private void listenForWholeDay(Observable obs) {
        if (!appRecWholedayTxtfield.isFocused()) {
            String wd = appRecWholedayTxtfield.getText();
            int wdInt = Integer.parseInt(wd);
            if (wdInt < 0 || wdInt > 1) {
                appRecWholedayTxtfield.setText("Invalid WholeDay Option - Please set a valid alarm (0 for false, 1 for true");
            }
            log.info("Lost focus");
        } else {
            log.info("Gained Focus");
        }
    }
}
