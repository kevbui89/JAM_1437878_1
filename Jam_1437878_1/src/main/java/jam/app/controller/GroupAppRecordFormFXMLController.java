package jam.app.controller;

import jam.business.AppGroupRecImpl;
import jam.business.IAppGroupRecDAO;
import jam.entities.AppGroupRecord;
import jam.entities.Configuration;
import jam.manager.PropertiesManager;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.util.converter.NumberStringConverter;
import org.slf4j.LoggerFactory;

/**
 * Group Appointment Record Form
 *
 * @author Kevin
 */
public class GroupAppRecordFormFXMLController {

    private final org.slf4j.Logger log = LoggerFactory.getLogger(this.getClass().getName());
    private IAppGroupRecDAO agrDAO;
    private AppGroupRecord agr;
    private final String pattern = "^#[A-Z0-9]{6}$";
    private Configuration config;
    List<AppGroupRecord> list;
    private int groupCounter;
    private int maxGroup = 0;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField appGroupRecIdTxtfield;

    @FXML
    private TextField appGroupRecGroupnameTxtfield;

    @FXML
    private TextField appGroupRecGroupcolorTxtfield;

    @FXML
    private Button appGroupRecAddBtn;

    @FXML
    private Button appGroupRecDeleteBtn;

    @FXML
    private Button appGroupRecSaveBtn;

    @FXML
    private Button clearGroupAppBtn;

    @FXML
    private Button appGroupRecPreviousBtn;

    @FXML
    private Button AppGroupRecNextBtn;

    /**
     * Default Constructor
     *
     * @throws java.io.IOException
     */
    public GroupAppRecordFormFXMLController() throws IOException, SQLException {
        super();
        setUp();
        this.agrDAO = new AppGroupRecImpl(config);
        this.groupCounter = 0;
    }

    /**
     * Sets up the Configuration
     *
     * @throws java.io.IOException
     */
    public void setUp() throws IOException {
        PropertiesManager pm = new PropertiesManager();
        config = new Configuration();
        config = pm.loadTextProperties("src/main/resources", "JamDatabaseProperties", config);
    }

    /**
     * Bind text field properties to bean properties
     */
    private void doBindings() {
        log.info("Inside doBindings()");
        Bindings.bindBidirectional(appGroupRecIdTxtfield.textProperty(), agr.idProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(appGroupRecGroupnameTxtfield.textProperty(), agr.groupNameProperty());
        Bindings.bindBidirectional(appGroupRecGroupcolorTxtfield.textProperty(), agr.colorProperty());
    }

    @FXML
    void addGroupRec(ActionEvent event) {
        doBindings();
        log.debug("AGR to create: " + agr);
        try {
            agrDAO.createAppGroupRec(agr);
        } catch (SQLException ex) {
            log.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    @FXML
    void deleteAppGroupRec(ActionEvent event) {
        doBindings();
        log.debug("AGR to delete: " + agr);
        try {
            agrDAO.deleteAppGroupRec(agr);
        } catch (SQLException ex) {
            log.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    @FXML
    void clearGroupApp(ActionEvent event) {
        agr.setId(0);
        agr.setGroupName("");
        agr.setColor("");
    }

    @FXML
    void nextAppGroupRec(ActionEvent event) {
        if (!list.isEmpty()) {
            groupCounter++;
            log.info("groupCounter: " + groupCounter);
            log.info("maxGroup: " + maxGroup);
            if (groupCounter >= maxGroup) {
                agr = list.get(groupCounter);
                doBindings();
                groupCounter = -1;
            } else {
                agr = list.get(groupCounter);
                doBindings();
            }
        }
    }

    @FXML
    void previousAppGroupRec(ActionEvent event) {
        if (!list.isEmpty()) {
            groupCounter--;
            log.info("groupCounter: " + groupCounter);
            log.info("maxGroup: " + maxGroup);
            if (groupCounter <= -1) {
                groupCounter = maxGroup;
                agr = list.get(groupCounter);
                doBindings();
            } else {
                agr = list.get(groupCounter);
                doBindings();
            }
        }

    }

    @FXML
    void saveAppGroupRec(ActionEvent event) {
        doBindings();
        log.debug("AGR to update: " + agr);
        try {
            agrDAO.updateAppGroupRec(agr);
        } catch (SQLException ex) {
            log.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    @FXML
    void initialize() {
        appGroupRecGroupcolorTxtfield.focusedProperty().addListener(this::listenForColor);
    }

    /**
     * Sets all the groups into a list
     *
     * @throws java.sql.SQLException
     */
    public void setAppGroupRecords() throws SQLException {
        list = agrDAO.findAppGroupRec();
        this.maxGroup = list.size() - 1;
    }

    /**
     * Sets the first group record to display on the form
     *
     * @throws SQLException
     */
    public void setFirstAppGroupRecord() throws SQLException {
        setAppGroupRecords();
        log.info("FIRST GROUP: " + list.get(0));
        if (!list.isEmpty()) {
            log.info("List is not empty");
            this.agr = list.get(0);
            doBindings();
        }
    }

    /**
     * Error pop up message
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(ResourceBundle.getBundle("MessageBundle").getString("ERRORDAO"));
        dialog.setHeaderText(ResourceBundle.getBundle("MessageBundle").getString("ERRORDAOTEXT"));
        dialog.setContentText(msg);
        dialog.show();
    }

    /**
     * Listens to check if the color is valid
     *
     * @param obs
     */
    private void listenForColor(Observable obs) {
        String clr = "";
        if (!appGroupRecGroupcolorTxtfield.isFocused()) {
            clr = appGroupRecGroupcolorTxtfield.getText().toUpperCase();

            if (!clr.matches(pattern)) {
                appGroupRecGroupcolorTxtfield.setText("Invalid color - Please set a valid color in the format #A87459 (capitalized letters)");
            } else {
                appGroupRecGroupcolorTxtfield.setText(clr);
            }
            log.info("Lost focus");
        } else {
            log.info("Gained Focus");
        }
    }
}
