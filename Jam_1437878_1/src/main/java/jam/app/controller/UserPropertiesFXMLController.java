package jam.app.controller;

import jam.entities.Configuration;
import jam.manager.PropertiesManager;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.slf4j.LoggerFactory;

/**
 * Properties Controller
 *
 * @author Kevin
 */
public class UserPropertiesFXMLController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField usernameTxtfield;

    @FXML
    private TextField passwordTxtfield;

    @FXML
    private TextField urlTxtfield;

    @FXML
    private Button saveCredentialsBtn;

    private Configuration config;
    private final org.slf4j.Logger log = LoggerFactory.getLogger(this.getClass().getName());

    public UserPropertiesFXMLController() throws IOException {
        super();
        setUp();
    }

    /**
     * Sets up the Configuration
     */
    public void setUp() throws IOException {
        PropertiesManager pm = new PropertiesManager();
        config = new Configuration();
        config = pm.loadTextProperties("src/main/resources", "JamDatabaseProperties", config);
    }

    @FXML
    void saveCredentials(ActionEvent event) {
        PropertiesManager pm = new PropertiesManager();
        try {
            pm.writeTextProperties("src/main/resources", "JamDatabaseProperties", config);
        } catch (IOException ex) {
            log.error(null, ex);
            errorAlert("onSave()");
        }

    }

    @FXML
    void initialize() {
        Bindings.bindBidirectional(usernameTxtfield.textProperty(), config.userNameProperty());
        Bindings.bindBidirectional(passwordTxtfield.textProperty(), config.passwordProperty());
        Bindings.bindBidirectional(urlTxtfield.textProperty(), config.urlProperty());
    }

    /**
     * Error pop up message
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(ResourceBundle.getBundle("MessageBundle").getString("ERRORDAO"));
        dialog.setHeaderText(ResourceBundle.getBundle("MessageBundle").getString("ERRORDAOTEXT"));
        dialog.setContentText(msg);
        dialog.show();
    }
}
