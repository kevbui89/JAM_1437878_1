package jam.app.controller;

import jam.business.AppGroupRecImpl;
import jam.business.IAppGroupRecDAO;
import jam.business.ISmtpDAO;
import jam.business.SmtpImpl;
import jam.entities.AppGroupRecord;
import jam.entities.Configuration;
import jam.entities.Smtp;
import jam.manager.PropertiesManager;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.slf4j.LoggerFactory;

/**
 * Main Layout
 *
 * @author Kevin
 */
public class MainLayoutFXMLController {

    private final org.slf4j.Logger log = LoggerFactory.getLogger(this.getClass().getName());

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Menu fileMenu;

    @FXML
    private MenuItem closeMenu;

    @FXML
    private Menu helpMenu;

    @FXML
    private MenuItem aboutMenu;

    @FXML
    private Menu optionMenu;

    @FXML
    private MenuItem propertiesOption;

    @FXML
    private MenuItem smtpOptionMenu;

    @FXML
    private Button monthlyViewBtn;

    @FXML
    private Button weeklyViewBtn;

    @FXML
    private Button dailyViewBtn;

    @FXML
    private MenuItem appGroupOption;
    
     @FXML
    private Menu searchOption;

    @FXML
    private MenuItem searchAppByTitle;

    private Configuration config;
    private IAppGroupRecDAO agrDAO;
    private ISmtpDAO smtpDAO;
    private Smtp smtp;

    /**
     * Default constructor
     */
    public MainLayoutFXMLController() throws IOException {
        super();
        setUp();
        this.agrDAO = new AppGroupRecImpl(config);
        this.smtpDAO = new SmtpImpl(config);
        this.smtp = new Smtp();
        log.info("Default Constructor");
    }

    /**
     * Sets up the Configuration
     */
    public void setUp() throws IOException {
        PropertiesManager pm = new PropertiesManager();
        config = new Configuration();
        config = pm.loadTextProperties("src/main/resources", "JamDatabaseProperties", config);
    }

    @FXML
    void handleAbout(ActionEvent event) {
        displayDialog("Application made by Kevin Bui");
    }

    @FXML
    void handleClose(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    void viewAppGroup(ActionEvent event) throws IOException, SQLException {
        log.debug("In viewAppGroup View");
        try {
            List<AppGroupRecord> list = agrDAO.findAppGroupRec();
        } catch (SQLException e) {
            log.debug(e.getMessage());
        }
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/GroupAppRecordFormFXML.fxml"));
        loader.setResources(ResourceBundle.getBundle("MessageBundle"));
        Parent root = (DialogPane) loader.load();
        GroupAppRecordFormFXMLController controller = loader.getController();
        controller.setFirstAppGroupRecord();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.show();
    }

    @FXML
    void showDailyView(MouseEvent event) throws IOException {
        log.debug("In Daily View");
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/DailyViewFXML.fxml"));
        loader.setResources(ResourceBundle.getBundle("MessageBundle"));
        Parent root = (BorderPane) loader.load();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.show();
    }

    @FXML
    void showMonthlyView(MouseEvent event) throws IOException {
        log.debug("In Monthly View");
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/MonthlyViewFXML.fxml"));
        loader.setResources(ResourceBundle.getBundle("MessageBundle"));
        Parent parent = (BorderPane) loader.load();
        Stage stage = new Stage();
        stage.setScene(new Scene(parent));
        stage.show();
    }

    @FXML
    void showWeeklyView(MouseEvent event) throws IOException {
        log.debug("In Weekly View");
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/WeeklyViewFXML.fxml"));
        loader.setResources(ResourceBundle.getBundle("MessageBundle"));
        Parent parent = (BorderPane) loader.load();
        Stage stage = new Stage();
        stage.setScene(new Scene(parent));
        stage.show();
    }

    @FXML
    void handleSmtpOption(ActionEvent event) throws IOException {
        log.debug("In Smtp Form");
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(this.getClass().getResource("/fxml/SmtpFormFXML.fxml"));
        loader.setResources(ResourceBundle.getBundle("MessageBundle"));
        Parent root = (DialogPane) loader.load();
        SmtpFormFXMLController controller = loader.getController();
        controller.setSmtpDAOData();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.show();
    }

    @FXML
    void initialize() {

    }

    @FXML
    void openPropertiesForm(ActionEvent event) throws IOException {
        log.debug("In User Properties Form");
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(this.getClass().getResource("/fxml/UserPropertiesFXML.fxml"));
        loader.setResources(ResourceBundle.getBundle("MessageBundle"));
        Parent root = (BorderPane) loader.load();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.show();
    }
    
    @FXML
    void openSearchBox(ActionEvent event) throws IOException {
        log.debug("In Search Form");
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(this.getClass().getResource("/fxml/AppSearchByTitleFXML.fxml"));
        loader.setResources(ResourceBundle.getBundle("MessageBundle"));
        Parent root = (DialogPane) loader.load();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.show();
    }
    
    /**
     * Message popup dialog
     *
     * @param msg
     */
    private void displayDialog(String msg) {
        Alert dialog = new Alert(Alert.AlertType.INFORMATION);
        dialog.setTitle(ResourceBundle.getBundle("MessageBundle").getString("ABOUT"));
        dialog.setHeaderText(ResourceBundle.getBundle("MessageBundle").getString("ABOUT"));
        dialog.setContentText(msg);
        dialog.show();
    }


}
