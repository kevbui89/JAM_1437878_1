package jam.app.controller;

import jam.business.ISmtpDAO;
import jam.business.SmtpImpl;
import jam.entities.Configuration;
import jam.entities.Smtp;
import jam.manager.PropertiesManager;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.util.converter.NumberStringConverter;
import org.slf4j.LoggerFactory;

/**
 * Smtp Form
 * @author Kevin
 */

public class SmtpFormFXMLController {
    
    private final org.slf4j.Logger log = LoggerFactory.getLogger(this.getClass().getName());
    private ISmtpDAO smtpDAO;
    private Smtp smtp;
    private final String pattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private Configuration config;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button smtpAddBtn;

    @FXML
    private Button deleteSmtpBtn;

    @FXML
    private Button saveSmtpBtn;

    @FXML
    private Button clearSmtpBtn;

    @FXML
    private Button previousSmtpBtn;

    @FXML
    private Button nextSmtpBtn;

    @FXML
    private TextField smtpUsernameTxtfield;

    @FXML
    private TextField smtpEmailTxtfield;

    @FXML
    private TextField smtpPasswordTxtfield;

    @FXML
    private TextField smtpUrlTxtfield;

    @FXML
    private TextField smtpPortnoTxtfield;

    @FXML
    private TextField smtpReminderTxtfield;

    @FXML
    private TextField smtpDefaultTxtfield;
    
    /**
     * Default Constructor
     */
    public SmtpFormFXMLController() throws IOException {
        super();
        setUp();
        this.smtpDAO = new SmtpImpl(config);
    }
    
    /**
     * Sets up the Configuration
     */
    public void setUp() throws IOException {
        PropertiesManager pm = new PropertiesManager();
        config = new Configuration();
        config = pm.loadTextProperties("src/main/resources", "JamDatabaseProperties", config);
    }
    
    /**
     * Bind the smtp text fields to the smtp bean properties
     */
    private void doBindings() {
        Bindings.bindBidirectional(smtpUsernameTxtfield.textProperty(), smtp.userNameProperty());
        Bindings.bindBidirectional(smtpEmailTxtfield.textProperty(), smtp.emailProperty());
        Bindings.bindBidirectional(smtpPasswordTxtfield.textProperty(), smtp.passwordProperty());
        Bindings.bindBidirectional(smtpUrlTxtfield.textProperty(), smtp.urlProperty());
        Bindings.bindBidirectional(smtpPortnoTxtfield.textProperty(), smtp.portNumberProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(smtpReminderTxtfield.textProperty(), smtp.reminderProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(smtpDefaultTxtfield.textProperty(), smtp.defaultSmtpProperty(), new NumberStringConverter());
    }
    
    @FXML
    void clearFields(ActionEvent event) {
        smtp.setDefaultSmtp(0);
        smtp.setEmail("");
        smtp.setPassword("");
        smtp.setPortNumber(0);
        smtp.setReminder(0);
        smtp.setUrl("");
        smtp.setUsername("");
    }

    @FXML
    void addSmtp(ActionEvent event) {
        doBindings();
        log.debug("SMTP to create: " + smtp);
        try {
            this.smtpDAO = smtpDAO;
            smtpDAO.createSmtp(smtp);
        } catch (SQLException ex) {
            log.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    @FXML
    void deleteSmtp(ActionEvent event) {
        doBindings();
        log.debug("SMTP to delete: " + smtp);
        try {
            this.smtpDAO = smtpDAO;
            smtpDAO.deleteSmtp(smtp);
        } catch (SQLException ex) {
            log.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    @FXML
    void saveSmtp(ActionEvent event) {
        doBindings();
        log.debug("SMTP to update: " + smtp);
        try {
            this.smtpDAO = smtpDAO;
            smtpDAO.updateSmtp(smtp);
        } catch (SQLException ex) {
            log.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    @FXML
    void initialize() {
        smtpEmailTxtfield.focusedProperty().addListener(this::listenForEmail);
        smtpDefaultTxtfield.focusedProperty().addListener(this::listenForSmtp);
        smtpReminderTxtfield.focusedProperty().addListener(this::listenForReminder);
    }

    public void setSmtpDAOData() {
        try {
            this.smtp = smtpDAO.findDefaultSmtp();
            doBindings();
        } catch (SQLException ex) {
            log.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }
    
    /**
     * Error pop-up message
     * @param msg 
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(ResourceBundle.getBundle("MessageBundle").getString("ERRORDAO"));
        dialog.setHeaderText(ResourceBundle.getBundle("MessageBundle").getString("ERRORDAOTEXT"));
        dialog.setContentText(msg);
        dialog.show();
    }
    
    /**
     * Listens to check if the email is valid
     * @param obs 
     */
    private void listenForEmail(Observable obs) {
        if (!smtpEmailTxtfield.isFocused()) {
            if (smtpEmailTxtfield.getText().matches(pattern)) {
                smtpEmailTxtfield.setText("Invalid Email - Please set a valid email");
            }
            log.info("Lost focus");
        } else {
            log.info("Gained Focus");
        }
    }
    
    /**
     * Listens and checks if the Smtp is valid
     * @param obs 
     */
    private void listenForSmtp(Observable obs) {
        if (!smtpDefaultTxtfield.isFocused()) {
            String smtp = smtpDefaultTxtfield.getText();
            int smtpInt = Integer.parseInt(smtp);
            if (smtpInt < 0 || smtpInt > 1) {
                smtpDefaultTxtfield.setText("Invalid Smtp - Please set a valid Smtp (0 for non default, 1 for default");
            }
            log.info("Lost focus");
        } else {
            log.info("Gained Focus");
        }
    }
    
    /**
     * Listens and checks if the reminder is valid
     * @param obs 
     */
    private void listenForReminder(Observable obs) {
        if (!smtpReminderTxtfield.isFocused()) {
            String reminder = smtpReminderTxtfield.getText();
            int reminderInt = Integer.parseInt(reminder);
            if (reminderInt < 0 || reminderInt > 1440) {
                smtpReminderTxtfield.setText("Invalid reminder - Please set a valid reminder (from 0 to 1440");
            }
            log.info("Lost focus");
        } else {
            log.info("Gained Focus");
        }
    }
}