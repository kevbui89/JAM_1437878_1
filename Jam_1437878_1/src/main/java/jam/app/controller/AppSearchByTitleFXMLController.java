package jam.app.controller;

import jam.entities.AppRecord;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FXML Controller class
 *
 * @author Kevin
 */
public class AppSearchByTitleFXMLController {
    
    private final Logger log = LoggerFactory.getLogger(this.getClass()
            .getName());
    private AppRecord ar;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField titleTxtfield;

    @FXML
    private Button searchBtn;
    
    public AppSearchByTitleFXMLController() {
        this.ar = new AppRecord();
    }

    @FXML
    void searchAppointment(ActionEvent event) throws IOException{
        String userInput = titleTxtfield.getText();
        ar.setTitle(userInput);
        dialogSearch(ar);
    }
    
    /**
     * Display the string contents of a single cell.
     *
     * @param data
     */
    private void dialogSearch(AppRecord data) throws IOException {
        log.debug("In AppRec Form");

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AppRecordFormFXML.fxml"));
        loader.setResources(ResourceBundle.getBundle("MessageBundle"));
        Parent root = (DialogPane) loader.load();
        AppRecordFormFXMLController controller = loader.getController();
        controller.setAppSearch(ar);
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.show();
    }

    @FXML
    void initialize() {
        
    } 
    
}
