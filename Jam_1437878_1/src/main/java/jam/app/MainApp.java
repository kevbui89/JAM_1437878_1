package jam.app;

import jam.app.controller.MainLayoutFXMLController;
import jam.app.controller.UserPropertiesFXMLController;
import jam.business.AppGroupRecImpl;
import jam.business.AppRecImpl;
import jam.business.IAppGroupRecDAO;
import jam.business.IAppRecDAO;
import jam.business.ISmtpDAO;
import jam.business.SmtpImpl;
import jam.entities.AppGroupRecord;
import jam.entities.AppRecord;
import jam.entities.Configuration;
import jam.entities.Smtp;
import jam.joddmailsendreceive.EmailTask;
import jam.manager.PropertiesManager;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kevin
 */
public class MainApp extends Application {

    private final Logger log = LoggerFactory.getLogger(this.getClass()
            .getName());
    private Stage primaryStage;
    private final Locale currentLocale;
    private ISmtpDAO smtpDAO;
    private Smtp smtp;
    private IAppGroupRecDAO agrDAO;
    private AppGroupRecord agr;
    private IAppRecDAO arDAO;
    private AppRecord ar;
    private Configuration config;
    private EmailTask emailTask;
    private ScheduledExecutorService executor;


    public static void main(String[] args) throws SQLException {
        launch(args);   
    } // End main

    public MainApp() throws SQLException, IOException {
        super();
        currentLocale = new Locale("en", "CA");
        setUp();
        log.debug("Locale= " + currentLocale);
        smtpDAO = new SmtpImpl(config);
        smtp = new Smtp();
        agrDAO = new AppGroupRecImpl(config);
        agr = new AppGroupRecord();
        arDAO = new AppRecImpl(config);
        ar = new AppRecord();
        emailTask = new EmailTask();
        executor = Executors.newScheduledThreadPool(1);
    }
    
    /**
     * Sets up the configuration
     */
    public void setUp() throws IOException {       
        PropertiesManager pm = new PropertiesManager();
        config = new Configuration();
        config = pm.loadTextProperties("src/main/resources", "JamDatabaseProperties", config);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        
        this.primaryStage = primaryStage;
        Scene scene1 = createMainLayout();
        Scene scene2 = createPropertiesForm();
        if (checkForProperties()) {
            this.primaryStage.setScene(scene1);
        } else {
            this.primaryStage.setScene(scene2);
        }
        this.primaryStage.show();
        
        this.primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                log.debug("MainApp is closing");
                executor.shutdown();
                Platform.exit();
            }
        });
        
        executor.scheduleWithFixedDelay(emailTask, 60, 60, TimeUnit.SECONDS);
    }

    /**
     * Creates the properties form
     * @return  The properties form
     * @throws Exception 
     */
    public Scene createPropertiesForm() throws Exception {
        FXMLLoader loader = new FXMLLoader();

        loader.setLocation(this.getClass().getResource("/fxml/UserPropertiesFXML.fxml"));
        loader.setResources(ResourceBundle.getBundle("MessageBundle"));
        Parent root = (BorderPane) loader.load();
        UserPropertiesFXMLController controller = loader.getController();
        Scene scene = new Scene(root);
        return scene;
    }

    /**
     * Creates the main layout of the agenda
     * @return  The main layout
     * @throws Exception 
     */
    public Scene createMainLayout() throws Exception {
        FXMLLoader loader = new FXMLLoader();

        loader.setLocation(this.getClass().getResource("/fxml/MainLayoutFXML.fxml"));
        loader.setResources(ResourceBundle.getBundle("MessageBundle"));
        Parent root = (BorderPane) loader.load();
        MainLayoutFXMLController controller = loader.getController();
        Scene scene = new Scene(root);
        return scene;
    }

    /**
     * Error pop up message
     * @param msg 
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(ResourceBundle.getBundle("MessagesBundle").getString("ERRORTITLE"));
        dialog.setHeaderText(ResourceBundle.getBundle("MessagesBundle").getString("ERRORTITLE"));
        dialog.setContentText(msg);
        dialog.show();
    }
    
    /**
     * Check if a Properties file exists and can be loaded into a bean. This
     * does not verify that the contents of the bean fields are valid.
     *
     * @return found Have we been able to load the properties?
     */
    private boolean checkForProperties() {
        boolean found = false;
        Configuration config = new Configuration();
        PropertiesManager pm = new PropertiesManager();

        try {
            if (pm.loadTextPropertiesBoolean("src/main/resources", "JamDatabaseProperties", config)) {
                found = true;
            }
        } catch (IOException ex) {
            log.debug(ex.getMessage());
        }
        return found;
    }


}


