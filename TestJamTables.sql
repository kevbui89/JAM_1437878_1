USE TESTDB;

DROP TABLE SMTP_SETTINGS;
DROP TABLE APPOINTMENTGROUPRECORD;
DROP TABLE APPOINTMENTRECORD;

-- SMTP Table

CREATE TABLE SMTP_SETTINGS (
UserName varchar(30) NOT NULL,
UserEmail varchar(50) PRIMARY KEY,
UserEmailPassword varchar(50) NOT NULL,
SmtpUrl varchar(50) NOT NULL,
SmtpPortNumber int DEFAULT 465,
Reminder int DEFAULT 15,
DefaultSmtp int DEFAULT 0
);

-- Appointment Group Record Table

CREATE TABLE APPOINTMENTGROUPRECORD (
GroupNumber int NOT NULL AUTO_INCREMENT PRIMARY KEY,
GroupName varchar(30) NOT NULL,
Color varchar(7) NOT NULL
);

-- Appointment Record

CREATE TABLE APPOINTMENTRECORD (
AppointmentID int NOT NULL AUTO_INCREMENT PRIMARY KEY,
Title varchar(30) NOT NULL,
Loc varchar(30) NOT NULL,
StartTime timestamp NOT NULL,
EndTime timestamp NOT NULL,
Details varchar(200) DEFAULT 'No details',
WholeDay int DEFAULT 0,
AppointmentGroup INTEGER,
AlarmReminderReq INT DEFAULT 0
);

-- Inserting data into SMTP_SETTINGS

INSERT INTO SMTP_SETTINGS (UserName, UserEmail, UserEmailPassword, SmtpUrl, SmtpPortNumber, Reminder, DefaultSmtp) VALUES 
('Jam Test', 'jamproject.test123@gmail.com', 'jamtest123', 'smtp.gmail.com', DEFAULT, 120, 1),
('test', 'test@gmail.com', 'test', 'smtp.gmail.com', DEFAULT, 120, 1);

-- Inserting data into APPOINTMENTGROUPRECORD

INSERT INTO APPOINTMENTGROUPRECORD (GroupName, Color) VALUES
('Magicians', '#FF0000'),
('Ultimatum', '#0000FF'),
('Purple Power', '#800080'),
('Dawson College', '#ADD8E6'),
('Random Team', '#FFA500');

-- Inserting data into APPOINTMENT RECORD

INSERT INTO APPOINTMENTRECORD (Title, Loc, StartTime, EndTime, Details, WholeDay, AppointmentGroup, AlarmReminderReq) VALUES 
('Coffee break', 'Cafeteria', '2017-09-23 17:08:00', '2017-09-23 23:59:00', DEFAULT, 0, 1, 0),
('Coffee break', 'Lab', '2017-09-23 17:08:00', '2017-09-23 23:59:00', DEFAULT, 0, 1, 0),
('Gym session', 'Gym', '2017-09-23 17:08:00', '2017-09-23 23:59:00', DEFAULT, 0, 1, 0);

SELECT * FROM APPOINTMENTRECORD WHERE StartTime BETWEEN '2017-09-23 02:25:00' AND '2017-09-23 02:30:00';
